version: "3.7"

volumes:
  thesis_appdata:
  thesis_database:
  thesis_pgadmin:
  overpass-pdb:
  overpasslive_db:

networks:
  thesis_network: 
    name: "thesis_network"
    driver: bridge

services:
  postgis:
    image: cmihai/postgis
    container_name: postgis
    restart: unless-stopped
    ports:
      - '5432:5432'
    environment:
      POSTGRES_DB: "thesisdb"
      POSTGRES_USER: "postgres"
      POSTGRES_PASSWORD: "postgres"
    volumes:
    # Binding a Docker Managed Volume      
      - thesis_database:/var/lib/postgresql/data
    networks:
      - thesis_network

  pgadmin:
    container_name: pgadmin4
    image: dpage/pgadmin4
    restart: unless-stopped
    environment:
      PGADMIN_DEFAULT_EMAIL: admin@admin.com
      PGADMIN_DEFAULT_PASSWORD: root
    ports:
      - "5050:80"
    volumes:
      # Binding a Docker Managed Volume
      - thesis_pgadmin:/var/lib/postgresql/data
    links:
      - postgis
    networks:
      - thesis_network
  
  cms:
    container_name: "cms"
    build:
      context: ./thesis-cms
      dockerfile: Dockerfile
    ports:
      - "1337:1337"
    restart: unless-stopped
    environment:
      STRAPI_HOST: 0.0.0.0
      STRAPI_PORT: 1337
      JWT_SECRET: ${JWT_SECRET}
      API_TOKEN_SALT: ${API_TOKEN_SALT}
      API_KEYS: ${API_KEYS}
      INSTANCE_CONNECTION_NAME: postgis
      DATABASE_PORT: 5432
      DATABASE_NAME: "thesisdb"
      DATABASE_USERNAME: "postgres"
      DATABASE_PASSWORD: "postgres"
    networks:
      - thesis_network
    depends_on:
      - postgis
      - pgadmin

  webui:
    container_name: "webui"
    build:
      context: ./webui
      dockerfile: Dockerfile
    restart: unless-stopped
    ports:
      - "8899:8899"
    environment:
      APP_VER: "v1.0.0.0"
      BACKEND_CMS_ENDPOINT: cms
      ORS_APIKEY: ${ORS_APIKEY}
      ORS_LOCAL_ENDPOINT: "http://ors-local:8080"
    networks:
      - thesis_network
    depends_on:
      - cms
      - pgadmin
      - postgis
  

  # This OpenRouteService Instance will be created with the data before any changes were made to the routing configuration and the data was not modified as of Jan 20 2022.
  uncorrected-ors:
    container_name: uncorrected-ors
    ports:
      - 8080:8080
      - 9001:9001
    image: openrouteservice/openrouteservice:latest
    restart: unless-stopped
    build:
      context: ./openrouteservice
      dockerfile: Dockerfile
      args:
        ORS_CONFIG: ./conf/ors-config.json
        OSM_FILE: ./uncorrected_kreis1_20012022.osm.pbf # What The ORS will use to build the map graphs for routing
    user: "${ORS_UID:-0}:${ORS_GID:-0}"
    volumes:
      - ./appdata/uncorrected_orsdata/graphs:/ors-core/data/graphs
      - ./appdata/uncorrected_orsdata/elevation_cache:/ors-core/data/elevation_cache
      - ./appdata/uncorrected_orsdata/logs/ors:/var/log/ors
      - ./appdata/uncorrected_orsdata/logs/tomcat:/usr/local/tomcat/logs
      - ./conf:/ors-conf
      - ./osmdata/uncorrected_kreis1_20012022.osm.pbf:/ors-core/data/osm_file.pbf

    environment:
      - BUILD_GRAPHS=False  # Forces the container to rebuild the graphs, e.g. when PBF is changed
      - "JAVA_OPTS=-Djava.awt.headless=true -server -XX:TargetSurvivorRatio=75 -XX:SurvivorRatio=64 -XX:MaxTenuringThreshold=3 -XX:+UseG1GC -XX:+ScavengeBeforeFullGC -XX:ParallelGCThreads=4 -Xms1g -Xmx2g -Xmx3g -Xmx4g" # Forces the docker to give 4 GB of memory you need to configure from docker settings
      - "CATALINA_OPTS=-Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=9001 -Dcom.sun.management.jmxremote.rmi.port=9001 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Djava.rmi.server.hostname=localhost" 

# This OpenRouteService Instance will have all accessibility tags and a fully connected network
  corrected-ors:
      container_name: corrected-ors
      ports:
        - 8181:8080
        - 9002:9001
      image: openrouteservice/openrouteservice:latest
      restart: unless-stopped
      build:
        context: ./openrouteservice
        dockerfile: Dockerfile
        args:
          ORS_CONFIG: ./conf/ors-config.json
          OSM_FILE: ./corrected_kreis1_07062022.osm.pbf # What The ORS will use to build the map graphs for routing
      user: "${ORS_UID:-0}:${ORS_GID:-0}"
      volumes:
        - ./appdata/corrrected_orsdata/graphs:/ors-core/data/graphs
        - ./appdata/corrrected_orsdata/elevation_cache:/ors-core/data/elevation_cache
        - ./appdata/corrrected_orsdata/logs/ors:/var/log/ors
        - ./appdata/corrrected_orsdata/logs/tomcat:/usr/local/tomcat/logs
        - ./conf:/ors-conf
        - ./osmdata/corrected_kreis1_07062022.osm.pbf:/ors-core/data/osm_file.pbf

      environment:
        - BUILD_GRAPHS=False  # Forces the container to rebuild the graphs, e.g. when PBF is changed
        - "JAVA_OPTS=-Djava.awt.headless=true -server -XX:TargetSurvivorRatio=75 -XX:SurvivorRatio=64 -XX:MaxTenuringThreshold=3 -XX:+UseG1GC -XX:+ScavengeBeforeFullGC -XX:ParallelGCThreads=4 -Xms1g -Xmx2g -Xmx3g -Xmx4g" # Forces the docker to give 4 GB of memory you need to configure from docker settings
        - "CATALINA_OPTS=-Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=9001 -Dcom.sun.management.jmxremote.rmi.port=9001 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Djava.rmi.server.hostname=localhost"    
# This OpenRouteService Instance have all accessibility tags removed such as surfaces, inclines, width, kerbs but has a fully connected network
  noattribute-ors:
      container_name: noattribute-ors
      ports:
        - 8282:8080
        - 9003:9001
      image: openrouteservice/openrouteservice:latest
      restart: unless-stopped
      build:
        context: ./openrouteservice
        dockerfile: Dockerfile
        args:
          ORS_CONFIG: ./conf/ors-config.json
          OSM_FILE: ./noattribute_kreis1_07062022.osm.pbf # What The ORS will use to build the map graphs for routing
      user: "${ORS_UID:-0}:${ORS_GID:-0}"
      volumes:
        - ./appdata/noattribute_orsdata/graphs:/ors-core/data/graphs
        - ./appdata/noattribute_orsdata/elevation_cache:/ors-core/data/elevation_cache
        - ./appdata/noattribute_orsdata/logs/ors:/var/log/ors
        - ./appdata/noattribute_orsdata/logs/tomcat:/usr/local/tomcat/logs
        - ./conf:/ors-conf
        - ./osmdata/noattribute_kreis1_07062022.osm.pbf:/ors-core/data/osm_file.pbf

      environment:
        - BUILD_GRAPHS=False  # Forces the container to rebuild the graphs, e.g. when PBF is changed
        - "JAVA_OPTS=-Djava.awt.headless=true -server -XX:TargetSurvivorRatio=75 -XX:SurvivorRatio=64 -XX:MaxTenuringThreshold=3 -XX:+UseG1GC -XX:+ScavengeBeforeFullGC -XX:ParallelGCThreads=4 -Xms1g -Xmx2g -Xmx3g -Xmx4g" # Forces the docker to give 4 GB of memory you need to configure from docker settings
        - "CATALINA_OPTS=-Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=9001 -Dcom.sun.management.jmxremote.rmi.port=9001 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Djava.rmi.server.hostname=localhost"    

  overpass:
    # specify the image here
    image: wiktorn/overpass-api:0.7.56.9
    container_name: overpass_swiss    
    restart: unless-stopped
    # uncomment if you want to build the image yourself
    build:
      context: ./overpass
      dockerfile: Dockerfile
    ports:
      - 12456:80
    volumes:
      # use a docker managed volume
      - overpasslive_db:/db
    environment:
      - OVERPASS_META=yes
      - OVERPASS_MODE=init
      - OVERPASS_PLANET_URL=https://download.geofabrik.de/europe/switzerland-latest.osm.bz2 # This will be your OSM bz2 file.
      - OVERPASS_RULES_LOAD=10
      # Comment line OVERPASS_DIFF so No Update will be performed
      - OVERPASS_DIFF_URL=https://download.openstreetmap.fr/replication/europe/switzerland/zurich/minute/
      - OVERPASS_UPDATE_SLEEP=3600
      - OVERPASS_USE_AREAS=false
    # healthcheck:
    #   test: 
    #     - ["CMD-SHELL", "curl -qf 'http://localhost/api/interpreter?data=\[out:json\];node(1498834449);out;' | jq '.generator' |grep -q Overpass || exit 1"]
    #   start_period: 48h
  
  gcp-navigation:
    container_name: gcp-navigation
    restart: unless-stopped
    build:
      context: ./gcp-navigation
      dockerfile: Dockerfile
    ports:
      - 8200:8200
    environment:
      - GCP_API_KEY=AIzaSyCxkUMWHNxqsVPxKAZ2TgM3YHZm1Z1Xq54
    networks:
      - thesis_network

  

 

    