'use strict';

/**
 * poilocation service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::poilocation.poilocation');
