'use strict';

/**
 * poilocation router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::poilocation.poilocation');
