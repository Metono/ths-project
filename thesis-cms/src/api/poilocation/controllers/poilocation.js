'use strict';

/**
 *  poilocation controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::poilocation.poilocation');
