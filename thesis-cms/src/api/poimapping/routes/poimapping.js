'use strict';

/**
 * poimapping router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::poimapping.poimapping');
