'use strict';

/**
 *  poimapping controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::poimapping.poimapping');
