'use strict';

/**
 * poimapping service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::poimapping.poimapping');
