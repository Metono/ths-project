'use strict';

/**
 *  wac-corrected controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::wac-corrected.wac-corrected');
