'use strict';

/**
 * wac-corrected service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::wac-corrected.wac-corrected');
