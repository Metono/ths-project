'use strict';

/**
 * wac-corrected router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::wac-corrected.wac-corrected');
