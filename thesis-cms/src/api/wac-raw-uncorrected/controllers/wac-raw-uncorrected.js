'use strict';

/**
 *  wac-raw-uncorrected controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::wac-raw-uncorrected.wac-raw-uncorrected');
