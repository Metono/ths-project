'use strict';

/**
 * wac-raw-uncorrected router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::wac-raw-uncorrected.wac-raw-uncorrected');
