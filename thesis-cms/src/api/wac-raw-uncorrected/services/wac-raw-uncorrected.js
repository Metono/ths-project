'use strict';

/**
 * wac-raw-uncorrected service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::wac-raw-uncorrected.wac-raw-uncorrected');
