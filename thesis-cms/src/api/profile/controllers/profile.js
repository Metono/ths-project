'use strict';

/**
 *  profile controller
 */
const jwt_decode = require("jwt-decode");
const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::profile.profile', ({ strapi }) => ({
    async searchProfileByUserId(ctx) {
        try {
            let myResult = { data: {} }

            if (ctx.request && ctx.request.header && ctx.request.header.authorization) {
                let rawToken = ctx.request.headers.authorization;
                let myToken = rawToken.split(' ')[1];
                let decodedToken = jwt_decode(myToken);
                let myProfileQuery = `SELECT * FROM profiles WHERE userid = ${decodedToken.id}`;
                const myProfile = await strapi.db.connection.context.raw(myProfileQuery);

                if (myProfile) {
                    console.log(myProfile.rows[0]);
                    myResult.data = myProfile.rows[0]
                    ctx.body = myResult;
                    ctx.status = 200;
                }
            }



        } catch (err) {
            ctx.status = 404;
            ctx.body = err.message;
        }
    }
}));
