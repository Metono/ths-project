'use strict';

/**
 * poicontrollgroup router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::poicontrollgroup.poicontrollgroup');
