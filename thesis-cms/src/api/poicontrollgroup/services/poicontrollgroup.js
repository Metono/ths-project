'use strict';

/**
 * poicontrollgroup service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::poicontrollgroup.poicontrollgroup');
