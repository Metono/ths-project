'use strict';

/**
 *  poicontrollgroup controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::poicontrollgroup.poicontrollgroup');
