'use strict';

/**
 * wac-raw-noattribute router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::wac-raw-noattribute.wac-raw-noattribute');
