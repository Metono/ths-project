'use strict';

/**
 *  wac-raw-noattribute controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::wac-raw-noattribute.wac-raw-noattribute');
