'use strict';

/**
 * wac-raw-noattribute service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::wac-raw-noattribute.wac-raw-noattribute');
