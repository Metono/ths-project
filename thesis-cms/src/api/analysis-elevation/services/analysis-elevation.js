'use strict';

/**
 * analysis-elevation service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::analysis-elevation.analysis-elevation');
