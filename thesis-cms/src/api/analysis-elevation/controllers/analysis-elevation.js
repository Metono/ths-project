'use strict';

/**
 *  analysis-elevation controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::analysis-elevation.analysis-elevation');
