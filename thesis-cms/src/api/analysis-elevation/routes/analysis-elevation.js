'use strict';

/**
 * analysis-elevation router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::analysis-elevation.analysis-elevation');
