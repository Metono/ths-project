'use strict';

/**
 *  wac-uncorrected controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::wac-uncorrected.wac-uncorrected');
