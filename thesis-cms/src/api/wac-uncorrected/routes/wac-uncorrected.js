'use strict';

/**
 * wac-uncorrected router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::wac-uncorrected.wac-uncorrected');
