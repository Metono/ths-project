'use strict';

/**
 * wac-uncorrected service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::wac-uncorrected.wac-uncorrected');
