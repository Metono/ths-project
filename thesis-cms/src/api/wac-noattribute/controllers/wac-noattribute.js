'use strict';

/**
 *  wac-noattribute controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::wac-noattribute.wac-noattribute');
