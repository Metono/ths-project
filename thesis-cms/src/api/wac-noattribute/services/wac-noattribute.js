'use strict';

/**
 * wac-noattribute service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::wac-noattribute.wac-noattribute');
