'use strict';

/**
 * wac-noattribute router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::wac-noattribute.wac-noattribute');
