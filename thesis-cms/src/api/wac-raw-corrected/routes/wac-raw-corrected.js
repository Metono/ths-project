'use strict';

/**
 * wac-raw-corrected router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::wac-raw-corrected.wac-raw-corrected');
