'use strict';

/**
 *  wac-raw-corrected controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::wac-raw-corrected.wac-raw-corrected');
