'use strict';

/**
 * wac-raw-corrected service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::wac-raw-corrected.wac-raw-corrected');
