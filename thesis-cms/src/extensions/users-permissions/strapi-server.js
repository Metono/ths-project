const utils = require('@strapi/utils');
const { getService } = require("@strapi/plugin-users-permissions/server/utils");
const { sanitize } = utils;
const sanitizeOutput = (user, ctx) => {
    const schema = strapi.getModel('plugin::users-permissions.user');
    const { auth } = ctx.state;
    return sanitize.contentAPI.output(user, schema, { auth });
};


module.exports = (plugin) => {


    return plugin;
};