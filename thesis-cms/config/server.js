module.exports = ({ env }) => ({
  host: env("STRAPI_HOST", "0.0.0.0"),
  port: env.int("STRAPI_PORT", 1337),
  app: {
    keys: env.array("APP_KEYS", ["testKey1", "testKey2", "testKey2"]),
  },
  url: env("STRAPI_BACKEND_BASEURL", "http://localhost:1337"),
  admin: {
    url: env('PUBLIC_ADMIN_URL','/admin'),
    serverAdminPanel: env.bool('SERVE_ADMIN', true)
  }
});
