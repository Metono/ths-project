const devConnection = {
  host: process.env.INSTANCE_CONNECTION_NAME,
  port: process.env.DATABASE_PORT,
  database: process.env.DATABASE_NAME,
  user: process.env.DATABASE_USERNAME,
  password: process.env.DATABASE_PASSWORD
};

const prodConnection = {
  host: `/cloudsql/${process.env.INSTANCE_CONNECTION_NAME}`,
  database: process.env.DATABASE_NAME,
  user: process.env.DATABASE_USERNAME,
  password: process.env.DATABASE_PASSWORD,
};

module.exports = ({ env }) => ({
  connection: {
    client: 'postgres',
    connection: env('NODE_ENV') === 'development' ? devConnection : prodConnection,
    options: {
      pool: {
        min: 0,
        max: 15,
      }
    }
  },
});
