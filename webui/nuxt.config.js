import colors from "vuetify/es5/util/colors";
const lightTheme = {
  primary: "#005cb9", // change header color from here || "#1e88e6", "#21c1d6", "#fc4b6c", "#563dea", "#9C27b0", "#ff9800"
  info: "#21c1d6",
  success: "#93d500",
  accent: "#fc4b6c",
  warning: "#ffb22a",
  default: "#563dea",
  background: "EEF5F9",
};
const darkTheme = {
  primary: "#005cb9", // change header color from here || "#1e88e6", "#21c1d6", "#fc4b6c", "#563dea", "#9C27b0", "#ff9800"
  info: "#21c1d6",
  success: "#93d500",
  accent: "#fc4b6c",
  warning: "#ffb22a",
  default: "#563dea",
};


export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - Route Optimization on Wheelchair and Visually Impaired Users',
    title: 'weMove',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    "vue-toastification/nuxt",
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/auth-next',
    'nuxt-leaflet',
  ],

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: process.env.BACKEND_CMS || 'http://localhost:1337',
  },


  env: {
    ORS_APIKEY: process.env.ORS_APIKEY || "",
    APP_VER: process.env.APP_VER || "v1.0.0.0",    
  },

  ssr: false,
  // Runtime Configurations
  publicRuntimeConfig: {
    ORS_APIKEY: process.env.ORS_APIKEY || "",
    projectTitle: "weMove : Mobility and Accessibility Application",
    ANALYSIS_CMS_ENDPOINT : process.env.ANALYSIS_CMS_ENDPOINT || "http://localhost:1337",
    UNCORRECTED_ORS_ENDPOINT : process.env.UNCORRECTED_ORS_ENDPOINT || "http://localhost:8080/ors",
    CORRECTED_ORS_ENDPOINT : process.env.CORRECTED_ORS_ENDPOINT || "http://localhost:8181/ors",
    NOATTRIBUTE_ORS_ENDPOINT : process.env.NOATTRIBUTE_ORS_ENDPOINT || "http://localhost:8282/ors",
    LOCAL_OVERPASS_ENDPOINT : process.env.LOCAL_OVERPASS_ENDPOINT || "http://localhost:12345",
    LIVE_ORS_ENDPOINT: process.env.LIVE_ORS_ENDPOINT || "",
    GCP_DIRECTION_ENDPOINT: process.env.GCP_DIRECTION_ENDPOINT || "http://localhost:8200",
  },

  privateRuntimeConfig: {

  },

  // Authentication
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {
            url:
              process.env.GATEWAY_LOGIN_URL || 'http://localhost:1337/api/auth/local',
              method: 'POST',
          },
          logout: false,
          user: {
            url: process.env.GATEWAY_PROFILE_URL || 'http://localhost:1337/api/users/me',
            method: 'GET',
          },
        },
        // tokenRequired: true,
        // tokenType: "Bearer",
        token: {
          property: "jwt",
          // global: true,
          required: true,
          type: "Bearer",
        },
        user: {
          // property: "email",
          property: false,
          autoFetch: true,
        },
      },
    },
    redirect: {
      login: "/login",
      callback: "/login",
      logout: "/login",
      home: "/simulation",
    },
  },
  toast: {
    timeout: 3000,
    closeOnClick: false
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
