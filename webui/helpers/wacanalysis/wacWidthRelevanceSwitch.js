/**
 * This function checks if the width value has relevance to the way
 * @param {*} widthValue = Width Value of the Segment
 * @returns 
 */
 module.exports.wacWidthRelevanceSwitch = function (widthValue) {
    try {
        if (parseFloat(widthValue) > 1.525) {
            return 0;
        } else {
            return 1;
        }
        
    } catch (err) {
        console.log("[wacWidthRelevanceSwitch] : " + err);
    }
}

