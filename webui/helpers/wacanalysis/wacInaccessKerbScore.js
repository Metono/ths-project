/**
 * 
 * @param {*} wk Weight of the Parameter
 * @param {String} ikc Kerb Inaccessibility Type Count
 * @param {Number} maxSegmentLength
 * @returns 
 */

module.exports.wacInaccessKerbScore = function (wk = 0.26, ikc, maxSegmentLength = 680) {
    try {
        let ck = 5 / maxSegmentLength;
        let total = (ikc * ck) * wk;
        if (isNaN(total)) {
            return 0;
        } else {
            return parseFloat(total.toFixed(6));
        }

    } catch (err) {
        console.error('wacInaccessKerbScore : ' + err);
    }
}

