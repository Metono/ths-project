/**
 * 
 * @param {*} payload Kerb Type Value
 * @returns 
 */
module.exports.wacKerbParser = function (payload) {
    try {
        let kerbCost;
        switch (payload) {
            case "":
                kerbCost = 0;
                break;
            case 'lowered':
                kerbCost = 0;
                break;
            case 'flush':
                kerbCost = 0;
                break;
            case 'lowered_and_sloped':
                kerbCost = 0;
                break;
            default:
                kerbCost = 1;
        }
        return kerbCost;
    } catch (err) {
        console.error(err);
    }
}

