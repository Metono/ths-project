/**
 * This function will calculate the L x (slope limit switch + width limit switch)
 * @param {*} L = Arbitrary large value 5
 * @param {*} slopeLimitSwitch = Computed Slope Limit Switch Value
 * @param {*} widthLimitSwitch = Computed Width Limit Switch Value
 * @returns 
 */
 module.exports.wacLSlopeLimitWidthLimit = function (L = 5, slopeLimitSwitch, widthLimitSwitch) {
    try {
        let total = L * (slopeLimitSwitch + widthLimitSwitch);
        return total;
    } catch (err) {
        console.log("[wacLSlopeLimitWidthLimit] : " + err);
    }
}

