
/**
 * This Function will just return the slope Coast = slopeVal / 0.05
 * @param {*} slopeVal 
 * @returns 
 */
module.exports.wacSlopeCostParser = function (slopeVal) {
    try {
        let total = (slopeVal/100) / 0.05; 
        return parseFloat(total);
    } catch (err) {
        console.error(err);
    }
}

