/**
 * This checks if the Width is Less than 0.815
 * @param {*} widthValue = Width Value of the Segment
 * @returns 
 */
 module.exports.wacWidthLimitSwitch = function (widthValue) {
    try {
        if (parseFloat(widthValue) < 0.815) {
            return 1;
        } else {
            return 0;
        }
        
    } catch (err) {
        console.log("[wacWidthLimitSwitch] : " + err);
    }
}

