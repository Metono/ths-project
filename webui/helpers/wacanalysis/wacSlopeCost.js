/**
 * The function returns a slope cost Slope percent value / 5 
 * @param {*} inclineVal 
 * @returns 
 */
module.exports.wacSlopeCost = function (inclineVal) {
    try {
        // Change to percentage value
        let total = (inclineVal / 100) / 0.05;
        if (isNaN(total)) {
            return 0;
        } else {
            return parseFloat(total.toFixed(6));
        }

    } catch (err) {
        console.error(err);
    }
}

