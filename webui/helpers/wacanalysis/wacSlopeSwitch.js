/**
 * 
 * @param {*} slopeValue = Inclination Value of the Segment
 * @returns 
 */
 module.exports.wacSlopeSwitch = function (slopeValue) {
    try {
        if (slopeValue > 5) {
            return 1;
        } else {
            return 0;
        }
        
    } catch (err) {
        console.log("[wacSlopeSwitch] : " + err);
    }
}

