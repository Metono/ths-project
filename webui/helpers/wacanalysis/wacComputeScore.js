/**
 * 
 * This function just adds and totals all parameters for the WAC
 * @param {*} p1 
 * @param {*} p2 
 * @param {*} p3 
 * @param {*} p4 
 * @param {*} p5 
 * @param {*} p6 
 * @param {*} p7 
 * @returns 
 */
module.exports.wacComputeScore = function (p1=0, p2=0, p3=0, p4=0, p5=0, p6=0, p7=0) {
  try {
    // console.log(p1, p2, p3, p4, p5, p6, p7);
    let total = p1 + p2 + p3 + p4 + p5 + p6 + p7;
    return parseFloat(total.toFixed(6));
  } catch (err) {
    console.error(err);
  }
}

