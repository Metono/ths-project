module.exports.wacCurrentLength = function (p1 = 0, p2 = 0) {
  try {
    let param1 = 0;
    let param2 = 0;
    if (isNaN(p1)) {
      param1 = 0;
    } else {
      param1 = p1;
    }
    if (isNaN(p2)) {
      param2 = 0;
    } else {
      param2 = p2;
    }
    let total = param1 - param2;

    return parseFloat(total.toFixed(6));

  } catch (err) {
    console.error(err);
  }
}

