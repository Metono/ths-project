/**
 * 
 * For The District 1 the longest way segment is at 680m
 * @param {*} w1 Weight Parameter
 * @param {String} crc Crossing Count
 * @param {*} maxSegmentLength This will be the Maximum Length of available way segments
 * @returns 
 */

module.exports.wacCrossingScore = function (w1 = 0.15, crc, maxSegmentLength) {
    try {
        let cc = 5 / maxSegmentLength;
        let total = w1 * (crc * cc);
        return parseFloat(total.toFixed(6));
    } catch (err) {
        console.error(err);
    }
}

