/**
 * This function will check if the slope is relevant enough
 * @param {*} slopeValue The inclination value of OSM
 * @returns 
 */
 module.exports.wacSlopeLimitSwitch = function (slopeValue) {
    try {
        if (slopeValue > 5) {
            return 1;
        } else {
            return 0;
        }
    } catch (err) {
        console.error(err);
    }
}

