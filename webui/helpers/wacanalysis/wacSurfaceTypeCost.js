/**
 * This function will just return the cost value of the surface type
 * @param {*} surfaceVal 
 * @returns 
 */
module.exports.wacSurfaceTypeCost = function (surfaceVal) {
    try {
        let surfaceCost;
        switch (surfaceVal) {
            case 'concrete':
                surfaceCost = 0.20;
                break;
            case 'asphalt':
                surfaceCost = 0.25;
                break;
            case 'paving_stones':
                surfaceCost = 0.30;
                break;
            case 'sett':
                surfaceCost = 0.30;
                break;
            case 'paved':
                surfaceCost = 0.30;
                break;
            case 'cobblestone':
                surfaceCost = 0.40;
                break;
            case 'concrete:plates':
                surfaceCost = 0.40;
                break;
            case 'grass_paver':
                surfaceCost = 0.50;
                break;
            case 'grass':
                surfaceCost = 0.60;
                break;
            case 'fine_gravel':
                surfaceCost = 0.80;
                break;
            case 'gravel':
                surfaceCost = 0.90;
                break;
            case 'sand':
                surfaceCost = 1;
                break;
            case 'unpaved':
                surfaceCost = 1;
                break;
            default:
                surfaceCost = 1;
        }
        return surfaceCost;
    } catch (err) {
        console.error(err);
    }
}

