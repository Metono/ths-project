module.exports.wacSlopeScore = function (w1=0.09, slopeVal, normalizedLength) {
    try {

        let totalVal = (w1 * (normalizedLength * slopeVal));
        return parseFloat(totalVal);
    } catch (err) {
        console.error(err);
    }
}

