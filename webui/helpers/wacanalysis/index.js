const { wacComputeScore } = require("../wacanalysis/wacComputeScore");
const { wacCrossingScore } = require("../wacanalysis/wacCrossingScore");
const { wacInaccessKerbScore } = require("../wacanalysis/wacInaccessKerbScore");
const { wacLength } = require("../wacanalysis/wacLength");
const { wacLengthNormalizer } = require("../wacanalysis/wacLengthNormalizer");
const { wacSlopeCost } = require("../wacanalysis/wacSlopeCost");
const { wacSlopeScore } = require("../wacanalysis/wacSlopeScore");
const { wacSlopeSwitch } = require("../wacanalysis/wacSlopeSwitch");
const { wacSlopeWidthLimit } = require("../wacanalysis/wacSlopeWidthLimit");
const { wacSurfaceCost } = require("../wacanalysis/wacSurfaceCost");
const { wacWidthCost } = require("../wacanalysis/wacWidthCost");
const { wacWidthLimitSwitch } = require("./wacWidthLimitSwitch");
const { wacWidthCostRelevance } = require("../wacanalysis/wacWidthCostRelevance");
const { ccScore } = require("../wacanalysis/ccScore");
const { ckScore } = require("../wacanalysis/ckScore");
const { wacCrossingParser } = require("../wacanalysis/wacCrossingParser");
const { wacKerbParser } = require("../wacanalysis/wacKerbParser");
const { wacCurrentLength } = require("../wacanalysis/wacCurrentLength");
const { wacWidthRelevanceSwitch } = require("../wacanalysis/wacWidthRelevanceSwitch");
const { wacSurfaceTypeCost } = require("../wacanalysis/wacSurfaceTypeCost");
const { wacSlopeCostParser } = require("../wacanalysis/wacSlopeCostParser");
const { wacSlopeLimitSwitch } = require("../wacanalysis/wacSlopeLimitSwitch");
const { wacLSlopeLimitWidthLimit } = require("../wacanalysis/wacLSlopeLimitWidthLimit");
const { wacInaccessibleKerbCount } = require("../wacanalysis/wacInaccessibleKerbCount");
const { wacCrossingCount } = require("../wacanalysis/wacCrossingCount");
module.exports = {
    wacCrossingCount:wacCrossingCount,
    wacInaccessibleKerbCount:wacInaccessibleKerbCount,
    wacLSlopeLimitWidthLimit:wacLSlopeLimitWidthLimit,
    wacSlopeLimitSwitch: wacSlopeLimitSwitch,
    wacSlopeCostParser: wacSlopeCostParser,
    wacSurfaceTypeCost: wacSurfaceTypeCost,
    wacWidthRelevanceSwitch: wacWidthRelevanceSwitch,
    wacCurrentLength: wacCurrentLength,
    wacKerbParser: wacKerbParser,
    wacCrossingParser: wacCrossingParser,
    ccScore: ccScore,
    ckScore: ckScore,
    wacSlopeCost: wacSlopeCost,
    wacSlopeWidthLimit: wacSlopeWidthLimit,
    wacWidthCost: wacWidthCost,
    wacComputeScore: wacComputeScore,
    wacCrossingScore: wacCrossingScore,
    wacSurfaceCost: wacSurfaceCost,
    wacWidthLimitSwitch: wacWidthLimitSwitch,
    wacSlopeSwitch: wacSlopeSwitch,
    wacLength: wacLength,
    wacInaccessKerbScore: wacInaccessKerbScore,
    wacLengthNormalizer: wacLengthNormalizer,
    wacSlopeScore: wacSlopeScore,
    wacWidthCostRelevance: wacWidthCostRelevance
}