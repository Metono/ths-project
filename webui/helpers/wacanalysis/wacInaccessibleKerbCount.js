/**
 * This Function Returns the Count of the Kerb based on Accessibility Type
 * @param {String} kerbType Type of Kerb
 * @param {Number}  
 * @returns 
 */

 module.exports.wacInaccessibleKerbCount = function (kerbType) {
    try {
        let kerbCost;
        switch (kerbType) {
            case "":
                kerbCost = 0;
                break;
            case 'lowered':
                kerbCost = 0;
                break;
            case 'flush':
                kerbCost = 0;
                break;
            default:
                kerbCost = 1;
        }
       return kerbCost
    } catch (err) {
        console.error('[wacInaccessibleKerbCount] ' + err);
    }
}

