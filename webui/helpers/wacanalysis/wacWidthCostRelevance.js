/**
 * 
 * @param {*} w1 Weight Assigned to Parameter
 * @param {*} wayLength Indicated way Value of the segment
 * @param {*} normalizedLength Normalized length of the segments in the database this is the largest value in the database for all segments
 * @param {*} widthVal calculated width cost
 * @returns 
 */
module.exports.wacWidthCostRelevance = function (w1 = 0.09, widthVal, wayLength, normalizedLength) {
    try {
        // Calculate Relevance Switch
        let wSwitch;
        if (widthVal === undefined || widthVal === null) {
            wSwitch = 0;
        }
        if (parseFloat(widthVal) > 1.525) {
            wSwitch = 0;
        } else {
            wSwitch = 1;
        }
        // Calculate Width Cost
        let wCost = parseFloat(widthVal) / (1.525 - 0.815);
        let normCalc = wayLength / normalizedLength;
        // Calculate Total Score
        let total = w1 * normCalc * wCost * wSwitch;
        if (isNaN(total)) {
            return 0;
        } else {
            return parseFloat(total.toFixed(6));
        }
    } catch (err) {
        console.error(err);
    }
}

