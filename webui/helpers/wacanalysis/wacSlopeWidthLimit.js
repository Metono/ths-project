/**
 * This function will calculate the L x (slope limit switch + width limit switch)
 * @param {*} L = Arbitrary large value 5
 * @param {*} slopeValue = Inclination Value of the Segment
 * @param {*} widthValue = Width Value of the Segment
 * @returns 
 */
module.exports.wacSlopeWidthLimit = function (L = 5, slopeValue, widthValue) {
    try {
        let slopeLimitSwitch;
        let widthLimitSwitch;
        if (slopeValue > 5) {
            slopeLimitSwitch = 1;
        } else {
            slopeLimitSwitch = 0;
        }

        if (widthValue < 0.815) {
            widthLimitSwitch = 1;
        } else {
            widthLimitSwitch = 0;
        }
        let total = L * (slopeLimitSwitch + widthLimitSwitch);
        return total;
    } catch (err) {
        console.log("[wacSlopeWidthLimit] : " + err);
    }
}

