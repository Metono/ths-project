/**
 * 
 * @param {*} w1 Weight of the Parameter
 * @param {*} wayLength Length of the way segment
 * @param {*} normalizedLength Normalized Length this is the largest length of the way segment in the database
 * @param {*} surfaceVal Surface Type of the segment
 * @returns 
 */
module.exports.wacSurfaceCost = function (w1 = 19, wayLength, normalizedLength, surfaceVal) {
    try { 
        let surfaceCost;
        switch (surfaceVal) {
            case 'concrete':
                surfaceCost = 0.20;
                break;
            case 'asphalt':
                surfaceCost = 0.25;
                break;
            case 'paving_stones':
                surfaceCost = 0.30;
                break;
            case 'sett':
                surfaceCost = 0.30;
                break;
            case 'paved':
                surfaceCost = 0.30;
                break;
            case 'cobblestone':
                surfaceCost = 0.40;
                break;
            case 'concrete:plates':
                surfaceCost = 0.40;
                break;
            case 'grass_paver':
                surfaceCost = 0.50;
                break;
            case 'grass':
                surfaceCost = 0.60;
                break;
            case 'fine_gravel':
                surfaceCost = 0.80;
                break;
            case 'gravel':
                surfaceCost = 0.90;
                break;
            case 'sand':
                surfaceCost = 1;
                break;
            case 'unpaved':
                surfaceCost = 1;
                break;
            default:
                surfaceCost = 1;
        }
        let calcLength = wayLength / normalizedLength;
        let total = (w1 * (calcLength * surfaceCost));
        if(isNaN(total)){
            return 0;
        }else{
            return parseFloat(total.toFixed(6));
        }
        
    } catch (err) {
        console.error(err);
    }
}

