/**
 * This function will calculate ws 	× NORMALIZED LENGTH × SLOPE COST 
 * @param {*} w1 Default weight is 0.14
 * @param {*} slopeCost 
 * @param {*} currentLength 
 * @param {*} totalLength Default is 680
 * @returns 
 */
module.exports.normalizedSlopeCost = function (ws = 0.14, slopeCost, currentLength, totalLength = 680) {
    try {
        let normalizedLength = currentLength / totalLength;
        let total = ws * normalizedLength * slopeCost;
        if (isNaN(total)) {
            return 0;
        } else {
            return parseFloat(total.toFixed(6));
        }

    } catch (err) {
        console.error('[normalizedSlopeCost] : ' + err);
    }
}

