
/**
 * This function will normalized the length of the given value
 * @param {*} wd Default 0.17
 * @param {*} currentLength Current value of the segment
 * @param {*} totalLength Max value of the segment
 * @returns 
 */
module.exports.normalizedLengthWeighted = function (wd = 0.17, currentLength, totalLength = 680) {
    try {
        let normalizedLength = currentLength / totalLength;
        let total = wd * normalizedLength;
        if (isNaN(total)) {
            return 0;
        } else {
            return parseFloat(total.toFixed(6));
        }
    } catch (err) {
        console.log("[normalizedLengthWeighted] : " + err);
    }
}

