
/**
 * This function calculates the ww	× NORMALIZED LENGTH × WIDTH COST × WIDTH RELEVANCE SWITCH 
 * @param {*} ww default weight 0.09
 * @param {*} currentLength Length of the segment
 * @param {*} totalLength Max Length of the segments Default 680 
 * @param {*} widthCost 
 * @param {*} widthRelevanceSwitch 
 * @returns 
 */
module.exports.normalizedWidthCostWidthRelevance = function (ww = 0.09, widthCost, widthRelevanceSwitch, currentLength, totalLength = 680) {
    try {
        let normalizedLength = currentLength / totalLength;
        let total = ww * normalizedLength * widthCost * widthRelevanceSwitch;
        if (isNaN(total)) {
            return 0;
        }
        else {
            return parseFloat(total.toFixed(6));
        }
    } catch (err) {
        console.log("[normalizedWidthCostWidthRelevance] : " + err);
    }
}

