
 /**
  * This function will normalized the length of the given value
  * @param {*} currentLength Current value of the segment
  * @param {*} totalLength Max value of the segment
  * @returns 
  */
 module.exports.normalizedLength = function (currentLength, totalLength = 680) {
    try {
        let total = currentLength / totalLength;
        if (isNaN(total)) {
            return 0;
        } else {
            return parseFloat(total.toFixed(6));
        }
    } catch (err) {
        console.log("[normalizedLength] : " + err);
    }
}

