/**
 * This function will calculate the (wt	× NORMALIZED LENGTH × SURFACE TYPE COST )
 * @param {*} wt Default at 0.17
 * @param {*} surfaceCost Surface Type Value
 * @param {*} currentLength Current Segment Length
 * @param {*} totalLength Default is 680
 * @returns 
 */
module.exports.normalizedSurfaceTypeCost = function (wt = 0.19, surfaceCost, currentLength, totalLength = 680) {
    try {
        let normalizedLength = currentLength / totalLength;
        let total = wt * surfaceCost * normalizedLength;
        return parseFloat(total.toFixed(6));
    } catch (err) {
        console.error('[normalizedSurfaceTypeCost] : ' + err);
    }
}

