const { normalizedLength } = require("../normalized/normalizedLength");
const { normalizedSlopeCost } = require("../normalized/normalizedSlopeCost");
const { normalizedSurfaceTypeCost } = require("../normalized/normalizedSurfaceTypeCost");
const { normalizedWidthCostWidthRelevance } = require("../normalized/normalizedWidthCostWidthRelevance");
const { normalizedLengthWeighted } = require("../normalized/normalizedLengthWeighted");
module.exports = {
    normalizedLength: normalizedLength,
    normalizedSlopeCost: normalizedSlopeCost,
    normalizedLengthWeighted: normalizedLengthWeighted,
    normalizedSurfaceTypeCost: normalizedSurfaceTypeCost,
    normalizedWidthCostWidthRelevance: normalizedWidthCostWidthRelevance
};