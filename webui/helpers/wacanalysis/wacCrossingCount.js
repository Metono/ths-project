/**
 * This Function checks if there is a crossing and returns a count
 * @param {String} crossingType Crossing Type
 * @returns 
 */

module.exports.wacCrossingCount = function (crossingType) {
    try {
        let crossingCost;
        switch (crossingType) {
            case 'zebra':
                crossingCost = 1;
                break;
            case 'marked':
                crossingCost = 1;
                break;
            case 'unmarked':
                crossingCost = 1;
                break;
            case 'uncontrolled':
                crossingCost = 1;
                break;
            default:
                crossingCost = 0;
        }

        return crossingCost
    } catch (err) {
        console.error('[wacCrossingCount] : ' + err);
    }
}
