/**
 * 
 * @param {*} w1 Weight Parameter
 * @param {*} wayLength Value of the Way Segment
 * @param {*} normalizedLength 
 * @returns 
 */
module.exports.wacLength = function (w1 = 0.17, wayLength, normalizedLength) {
    try {
        let calcLength = wayLength / normalizedLength;
        let total = (w1 * calcLength);
        if (isNaN(total)) {
            return 0;
        }
        else {            
            return parseFloat(total.toFixed(6));
        }
    } catch (err) {
        console.log("[wacLength] : " + err);
    }
}

