/**
 * 
 * @param {*} maxLength Max Length of the possible in all Way Segments in the database
 * @returns 
 */
 module.exports.ccScore = function (maxLength) {
    try {
        let total = 5 / maxLength;
        return parseFloat(total.toFixed(6));
    } catch (err) {
        console.error(err);
    }
}

