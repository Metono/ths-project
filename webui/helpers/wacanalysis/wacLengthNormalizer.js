/**
 * LengthNormalizer normalizes the value of the segment over the longest way segment available in the district.
 * For The District 1 the longest way segment is at 680m
 * @param {*} currentLength This will be the current length of the way segment
 * @param {*} totalLength This will be the Maximum Length of available way segments
 * @returns 
 */

module.exports.wacLengthNormalizer = function (currentLength, totalLength = 680) {
    try {
        let total = currentLength / totalLength;
        if (isNaN(total)) {
            return 0;
        } else {
            return parseFloat(total.toFixed(6));
        }
    } catch (err) {
        console.log("[wacLength] : " + err);
    }
}

