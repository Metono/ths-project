/**
 * This function will calculate the cost based on width value
 * @param {*} widthVal 
 * @returns 
 */
module.exports.wacWidthCost = function (widthVal) {
    try {
        let total = widthVal / (1.525 - 0.815)
        return parseFloat(total.toFixed(6));
    } catch (err) {
        console.error(err);
    }
}

