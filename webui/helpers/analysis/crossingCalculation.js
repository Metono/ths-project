module.exports.crossingCalculation = function (p1, w) {
    try{     
        if(p1 === undefined || p1 === null){
            return 0;
        }
        else{
            return p1 * w;
        }
       
    }catch(err){
        console.error(err);
    }
 }
 