/**
 * 
 * @param {*} p1 = Given Segment Surface
 * @param {*} p2 = User Preference Segment Limit
 * @param {*} w = Weight Factor
 * @returns 
 */

module.exports.surfaceCalculation = function (p1, p2, w) {
  try {
    if (p1 === undefined || p1 === null) {
      return 0;
    } else {
      return ((p1 / p2) * w)
    }

  } catch (err) {
    console.error(err);
  }
}
