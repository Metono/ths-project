/**
 * 
 * @param {*} p1 = Height Change Total
 * @param {*} p2 = Surface Parameter Total
 * @param {*} p3 = Total Accessibility Count 
 * @param {*} p4 = Crossing Total
 * @param {*} sl = Segment Distance
 * @returns 
 */
module.exports.raiCalculation = function (p1,p2,p3,p4,sl) {
    try{     
      return ((p1+p2+p3+p4)*sl);
    }catch(err){
        console.error(err);
    }
 }
 