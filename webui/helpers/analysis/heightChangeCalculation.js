/**
 * 
 * @param {*} p1 = Height Change 
 * @param {*} p2 = User Preference Height Change Limit
 * @param {*} w  = Weight
 * @returns 
 */

module.exports.heightChangeCalculation = function (p1, p2, w) {
  try {
    console.log("Calculating HCTV");
    console.log(p1,p2,w);
    if (p1 === undefined || p1 === null) {
      return 1*w;
    } else {
      return ((1 + (p1 / p2)) * w);
    }

  } catch (err) {
    console.error(err);
  }
}
