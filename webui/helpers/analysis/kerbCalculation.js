
/**
 * 
 * @param {*} p1 = Existing Kerb Count
 * @param {*} p2 = Existing Accessible Kerb Count
 * @param {*} w  = Weight
 * @returns 
 */
module.exports.kerbCalculation = function (p1,p2,w) {
    try{ 
        if(p1 === undefined || p1 === null){
            p1 = 0;
        } 
        if(p2 === undefined || p2 === null){
            p2 = 0;
        }        
        return ((p1-p2)*w)
    }catch(err){
        console.error(err);
    }
 }
 