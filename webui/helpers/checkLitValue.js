
/**
 * The checkLitValue function recevies a payload and returns a Boolean.
 * This will count all kerb barriers that have been processed
 * @param {String} payload This will contain the raw payload from the OSM
 * @returns {Boolean}
 */

 module.exports.checkLitValue = function (payload) {
    try {
      if (payload == 'yes') {
        return true;
      }
      else if (payload == 'no') {
        return false;
      }
      else if (payload == '') {
        return false;
      }else{
          return false;
      }
    } catch (err) {
      console.error(err);
    }
  }
  
  