
/**
 * The checkKerbScore function recevies a payload and returns a Number.
 * This will count all kerb barriers that have been processed
 * @param {String} payload This will contain the raw payload from the OSM
 * @returns {Integer}
 */

module.exports.checkKerbScore = function (payload) {
  try {
    if (payload == 'lowered') {
      return 1;
    }
    else if (payload == 'raised') {
      return 1;
    }
    else if (payload == 'flushed') {
      return 1;
    }
    else if (payload == 'rolled') {
      return 1;
    }
    else {
      return 0;
    }
  } catch (err) {
    console.error(err);
  }
}

