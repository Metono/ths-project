/**
 * The widthFormatter function recevies a payload and returns a Number.
 * This will just sanitize the width to get only the decimal value
 * @param {String} payload This will contain the raw payload from the OSM
 * @returns {Boolean}
 */

 module.exports.widthFormatter = function(payload) {
    try {
      if(payload === undefined || payload === null){
        return 2;
      }else{
        return parseFloat(payload);
      }     
    } catch (err) {
      console.error(err);
    }
  }
  