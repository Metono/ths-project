
/**
 * The checkCrossingScore function recevies a payload and returns a Number.
 * This will only count kerbs that are accessible and return a score
 * @param {String} payload This will contain the raw payload from the OSM
 * @returns {Integer}
 */

 module.exports.checkCrossingScore = function (payload) {
    try{     
        if(payload == 'crossing'){
            return 1;
          }
          else if(payload == 'zebra'){
            return 1;
          }
          else if(payload == 'unmarked'){
            return 1;
          }
          else{
            return 0;
          }
    }catch(err){
        console.error(err);
    }
 }
 
 