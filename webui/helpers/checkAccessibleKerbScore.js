
/**
 * The checkAccessibleKerbScore function recevies a payload and returns a Number.
 * This will only count kerbs that are accessible and return a score
 * @param {String} payload This will contain the raw payload from the OSM
 * @returns {Integer}
 */

 module.exports.checkAccessibleKerbScore = function (payload) {
    try{     
        if(payload == 'lowered'){
            return 1;
          }
        else if(payload == 'raised'){
            return 0;
        }
          else{
            return 0;
          }
    }catch(err){
        console.error(err);
    }
 }
 
 