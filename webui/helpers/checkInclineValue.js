
/**
 * The checkInclineValue function recevies a payload and returns a string.
 * @param {String} payload This will contain the raw payload from the OSM
 * @returns {Integer} The Accelorometer value.
 */

module.exports.checkInclineValue = function (payload) {
  try {

    // Using Optional Shortcut on Payload
    let checkPayload = payload?.toString() || '';

    if (payload == 'undefined' || payload == null) {
      return 15;
    }
    else if (payload == "up" || payload == "down") {
      return 15;
    }
    else if (checkPayload.includes("°")) {
      let processPayload = checkPayload.replace("°", "");
      let changePayloadType = parseInt(processPayload);
      return changePayloadType;
    }
    else if (checkPayload.includes("%")) {
      let processPayload = checkPayload.replace("%", "");
      let changePayloadType = parseInt(processPayload);
      return changePayloadType;
    }
    else {
      return payload;
    }
  } catch (err) {
    console.error(err);
  }
}

