/**
 * The checkSurfaceTypeScore function recevies a payload and returns a Number.
 * @param {String} payload This will contain the raw payload from the OSM
 * @returns {Integer} The Accelorometer value.
 */

module.exports.checkSurfaceTypeScore = function (payload) {
    try {

        if (payload == 'asphalt') {
            return 1;
        }
        else if (payload == 'paved') {
            return 1;
        }
        else if (payload == 'concrete') {
            return 1;
        }
        else if (payload == 'paving_stones') {
            return 3;
        }
        else if (payload == 'sett') {
            return 4;
        }
        else if (payload == 'cobblestone') {
            return 4;
        }
        else if (payload == 'unpaved') {
            return 6;
        }
        else if (payload == 'stepping_stones') {
            return 6;
        }
        else if (payload == 'gravel') {
            return 6;
        }
        else if (payload == 'grass') {
            return 6;
        }
        else {
            return 6;
        }
    } catch (err) {
        console.error(err);
        return 6;
    }
}
