/**
 * The function checks if there is a surface type given 
 * @param {String} payload This will contain the raw payload from the OSM
 * @returns {Integer} The Accelorometer value.
 */

 module.exports.checkSurfaceType = function (payload) {
    try {
        if(payload === null || payload === undefined){
            return "unpaved";
        }else{
            return payload;
        }
    } catch (err) {
        console.error(err);
        return "unpaved";
    }
}
