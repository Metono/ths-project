
/**
 * The checkKerbType function recevies a payload and returns a String.
 * @param {String} payload This will contain the raw payload from the OSM
 * @returns {String}
 */

 module.exports.checkKerbType = function (payload) {
    try{     
        if(payload){
            return payload;
        }
        else{
            return "";
        }
    }catch(err){
        console.error(err);
    }
 }
 
 