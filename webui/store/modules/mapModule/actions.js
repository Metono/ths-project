const axios = require("axios");
const _ = require("lodash");
const { widthFormatter } = require("./../../../helpers/widthFormatter");
const { crossingCalculation } = require("./../../../helpers/analysis/crossingCalculation");
const { heightChangeCalculation } = require("./../../../helpers/analysis/heightChangeCalculation");
const { kerbCalculation } = require("./../../../helpers/analysis/kerbCalculation");
const { raiCalculation } = require("./../../../helpers/analysis/raiCalculation");
const { surfaceCalculation } = require("./../../../helpers/analysis/surfaceCalculation");
const { checkInclineValue } = require("./../../../helpers/checkInclineValue");
const { checkSurfaceTypeScore } = require("./../../../helpers/checkSurfaceTypeScore");
const { checkKerbScore } = require("../../../helpers/checkKerbScore");
const { checkCrossingScore } = require("../../../helpers/checkCrossingScore");
const { checkAccessibleKerbScore } = require("./../../../helpers/checkAccessibleKerbScore");
const { checkCrossingType } = require("./../../../helpers/checkCrossingType");
const { checkLitValue } = require("./../../../helpers/checkLitValue");
const { checkKerbType } = require("../../../helpers/checkKerbType");
const { checkSurfaceType } = require("./../../../helpers/checkSurfaceType");
const { normalizedLength,
  normalizedSlopeCost,
  normalizedLengthWeighted,
  normalizedSurfaceTypeCost,
  normalizedWidthCostWidthRelevance
} = require("../../../helpers/wacanalysis/normalized");
const { wacComputeScore,
  wacLSlopeLimitWidthLimit,
  wacInaccessibleKerbCount,
  wacSlopeCostParser,
  wacCurrentLength,
  wacSurfaceTypeCost,
  wacKerbParser,
  wacCrossingParser,
  wacSlopeWidthLimit,
  wacCrossingScore,
  ccScore,
  ckScore,
  wacSurfaceCost,
  wacSlopeCost,
  wacSlopeSwitch,
  wacWidthLimitSwitch,
  wacWidthRelevanceSwitch,
  wacLength,
  wacWidthCost,
  wacInaccessKerbScore,
  wacLengthNormalizer,
  wacWidthCostRelevance,
  wacCrossingCount,
  wacSlopeLimitSwitch,
  wacSlopeScore } = require("./../../../helpers/wacanalysis");
import { v4 as uuidv4 } from 'uuid';
import openrouteservice from "openrouteservice-js";
import { decode, encode } from "@googlemaps/polyline-codec";
// Add your api_key here
const Directions = new openrouteservice.Directions({
  api_key: process.env.ORS_APIKEY,
});

export default {
  async SET_TOGGLE_START_ENDPOINT({ commit }, { curState }) {
    try {
      commit("TOGGLE_START_POINT_OPTION", { curState });
      console.log("Update Start Point State");
    } catch (err) {
      console.log(err);
    }
  },
  async SET_TOGGLE_END_ENDPOINT({ commit }, { curState }) {
    try {
      commit("TOGGLE_END_POINT_OPTION", { curState });
      console.log("Update End Point State");
    } catch (err) {
      console.log(err);
    }
  },

  async SET_START_MARKER({ commit }, { lat, lng }) {
    try {
      console.log("Setting Start Coordinates");
      commit("SET_START_MARKER_LOCATION", { lat, lng });
      commit("TOGGLE_START_POINT_OPTION", { curState: false });
      commit("PUSH_COORDINATE_MARKER_LOCATION", [lat, lng]);
    } catch (err) {
      console.log(err);
    }
  },
  async SET_END_MARKER({ commit }, { lat, lng }) {
    try {
      console.log("Setting End Coordinates");
      commit("SET_END_MARKER_LOCATION", { lat, lng });
      commit("TOGGLE_END_POINT_OPTION", { curState: false });
      commit("PUSH_COORDINATE_MARKER_LOCATION", [lat, lng]);
    } catch (err) {
      console.log(err);
    }
  },

  // Adding Barriers for Avoidance
  async TOGGLE_ADD_BARRIER_OPTION({ commit }) {
    try {
      console.log("Toggle Add Barrier Coordinates");
      commit("SET_BARRIER_TOGGLE_OPTION", { curState: true });
    } catch (err) {
      console.log(err);
      commit("SET_BARRIER_TOGGLE_OPTION", { curState: false });
    }
  },

  async TOGGLE_ADD_BARRIER_OPTION_OFF({ commit }) {
    commit("SET_BARRIER_TOGGLE_OPTION", { curState: false });
  },

  async CLEAR_EXISTING_BARRIERS({ commit }) {
    commit("CLEAR_ADDED_BARRIERS");
  },

  async ADD_BARRIER_OPTION({ commit }, { lat, lng }) {
    try {
      commit("SET_BARRIER_COORDINATES", { lat, lng })
    } catch (err) {
      console.log(err);
    }
  },

  async ADD_BUFFERED_BARRIER_COORDINATES({ commit }, payload) {
    try {
      commit("SET_BUFFERED_BARRIER_COORDINATES", payload);
    } catch (err) {
      console.log(err);
    }
  },

  // Uncorrected ORS Processing
  async PROCESS_LOCAL_PROFILE_ROUTE({ commit }, { markerList, profileChoice, avoidPolygon = [], maximum_incline = "20", maximum_sloped_kerb = "5", minimum_width = 0.815, surface_type = "cobblestone", routingType = "recommended" }) {

    // Default Raw Payloads  
    let lcl_rec_raw;

    // Storage of Way IDS
    let lcl_rec_wayids = [];

    // Storage of Tags Attributes
    let lcl_rec_tagattr = [];

    // Storage of Routing Coordinates
    let lcl_rec_routingCoord = [];

    // Storage of OSM Way Query;
    let lcl_rec_wayQuery = "";

    //  Create a Segment Location Map    
    //  this set will contain the way index of the osmid
    const nodeSegmentMap = new Set();
    const elevationSetList = new Set();
    const coordinatesSetList = new Set();
    const rawWacSetList = new Set();
    const lineMarkerSetList = new Set();
    const osmDistanceSetList = new Set();
    const wacDataSetList = new Set();
    const rawWacAnalysisSetList = new Set();

    // Restriction Preference of User
    let myRestrictions = {};
    let myRoutingOption = {
      profile_params: {
        restrictions: {
          maximum_incline: maximum_incline,
          maximum_sloped_kerb: maximum_sloped_kerb,
          minimum_width: minimum_width,
          surface_type: surface_type
        },
      },
      avoid_polygons: {
        type: "MultiPolygon",
        coordinates: avoidPolygon
      }
    };

    if (avoidPolygon.length == 0) {
      delete myRoutingOption['avoid_polygons'];
    }
    if (profileChoice != 'wheelchair') {
      delete myRoutingOption['profile_params'];
    }

    try {
      lcl_rec_raw = await axios.post(`${this.$config.UNCORRECTED_ORS_ENDPOINT}/v2/directions/${profileChoice}/geojson`, {
        coordinates: markerList,
        extra_info: ["osmid", "waytype", "steepness"],
        elevation: true,
        instructions_format: "html",
        language: "en",
        preference: routingType,
        options: myRoutingOption
      }
      );
      if (lcl_rec_raw) {


        // prettier-ignore
        for (let itr = 0; itr < lcl_rec_raw.data.features[0].geometry.coordinates.length; itr++) {
          coordinatesSetList.add({ id: itr, latitude: lcl_rec_raw.data.features[0].geometry.coordinates[itr][1], longtitude: lcl_rec_raw.data.features[0].geometry.coordinates[itr][0] });
          lcl_rec_routingCoord.push([lcl_rec_raw.data.features[0].geometry.coordinates[itr][1], lcl_rec_raw.data.features[0].geometry.coordinates[itr][0]]);
          elevationSetList.add(lcl_rec_raw.data.features[0].geometry.coordinates[itr][2]);
        }

        // prettier-ignore
        if (lcl_rec_raw.data.features[0].properties.extras.osmId) {


          for (let i = 0; i < lcl_rec_raw.data.features[0].properties.extras.osmId.values.length; i++) {

            osmDistanceSetList.add({
              osmid: lcl_rec_raw.data.features[0].properties.extras.osmId.summary[i].value,
              distance: lcl_rec_raw.data.features[0].properties.extras.osmId.summary[i].distance,
              amount: lcl_rec_raw.data.features[0].properties.extras.osmId.summary[i].amount
            });

            nodeSegmentMap.add({ osmid: lcl_rec_raw.data.features[0].properties.extras.osmId.values[i][2], startIndex: lcl_rec_raw.data.features[0].properties.extras.osmId.values[i][0], endIndex: lcl_rec_raw.data.features[0].properties.extras.osmId.values[i][1] });
            let myArr = lcl_rec_raw.data.features[0].properties.extras.osmId.values[i];
            lcl_rec_wayids.push(myArr.slice(-1).pop());
          }
          for (let iter in lcl_rec_wayids) {
            lcl_rec_wayQuery = lcl_rec_wayQuery + "way(" + lcl_rec_wayids[iter] + ");";
          }
          try {
            let myTagResult = await axios.get(`${this.$config.LOCAL_OVERPASS_ENDPOINT}/api/interpreter?data=[out:json];(${lcl_rec_wayQuery});out%20tags;`);
            if (myTagResult) {
              Object.assign(lcl_rec_tagattr, myTagResult.data.elements);
              commit("SET_REC_LCL_FEATURE_LIST", lcl_rec_tagattr);
            }
          } catch (err) {
            console.log("[PROCESS_LOCAL_PROFILE_ROUTE] Tag Result : " + err);
          }

        } else {
          console.log("[PROCESS_LOCAL_PROFILE_ROUTE] No OSM ID to be parsed.");
        }
      }
    } catch (err) {
      console.log('PROCESS_LOCAL_PROFILE_ROUTE : ' + err);
    }
    // prettier-ignore
    commit("SET_REC_LCL_RAWDATA", lcl_rec_raw.data);
    commit("SET_REC_LCL_ROUTING_COORDINATES", lcl_rec_routingCoord);
    commit("SET_REC_LCL_WAYNODE_LIST", lcl_rec_wayids);
    commit("SET_REC_LCL_ROUTE_INSTRUCTION", lcl_rec_raw.data.features[0].properties.segments[0].steps);
    commit("SET_REC_LCL_ROUTE_ELEVATION", [...elevationSetList]);

    try {

      // Counters for the Statistics     
      let tagCounter = _.size(lcl_rec_tagattr);

      for (let itr = 0; itr < tagCounter; ++itr) {
        rawWacSetList.add({
          osmid: lcl_rec_tagattr[itr].id,
          coordinates: lcl_rec_routingCoord[itr],
          surface: checkSurfaceType(lcl_rec_tagattr[itr].tags.surface),
          highway: lcl_rec_tagattr[itr].tags.highway,
          lit: checkLitValue(lcl_rec_tagattr[itr].tags.lit),
          crossing: checkCrossingType(lcl_rec_tagattr[itr].tags.crossing),
          incline: checkInclineValue(lcl_rec_tagattr[itr].tags.incline),
          kerb: checkKerbType(lcl_rec_tagattr[itr].tags.kerb),
          width: widthFormatter(lcl_rec_tagattr[itr].tags.width)
        });
      }

      // This step will create the Analysis Table based on the helper functions and store data to the vuex store.
      rawWacSetList.forEach(el => {
        // Search in the Segment osm  ID and assign lat long and assign the corresponding order
        let nodeStartIndex;
        let nodeEndIndex;
        let currentWayOrder;
        for (let e of nodeSegmentMap) {
          if (e.osmid === el.osmid) {
            nodeStartIndex = e.startIndex;
            nodeEndIndex = e.endIndex;
            currentWayOrder = e.startIndex;
            break;
          }
        }

        // Add the lat-lng value of the markers
        lineMarkerSetList.add([[...coordinatesSetList][nodeStartIndex].latitude, [...coordinatesSetList][nodeStartIndex].longtitude]);
        lineMarkerSetList.add([[...coordinatesSetList][nodeEndIndex].latitude, [...coordinatesSetList][nodeEndIndex].longtitude]);

        //  Search in Distance Set List the osmid and assign the corresponding distance of segment
        let currentOsmDistance;
        for (let d of osmDistanceSetList) {
          if (d.osmid === el.osmid) {
            currentOsmDistance = d.distance;
            break;
          }
        }

        // Set Data in the Object
        let currentOsmDetails = {};
        for (let x of rawWacSetList) {
          if (x.osmid === el.osmid) {
            currentOsmDetails.osmid = x.osmid;
            currentOsmDetails.surface = x.surface;
            currentOsmDetails.highway = x.highway;
            currentOsmDetails.lit = x.lit;
            currentOsmDetails.crossing = x.crossing;
            currentOsmDetails.incline = x.incline;
            currentOsmDetails.kerb = x.kerb;
            currentOsmDetails.width = x.width;
            currentOsmDetails.sWidth = wacWidthLimitSwitch(x.width);
            currentOsmDetails.sIncline = wacSlopeLimitSwitch(x.incline);
            currentOsmDetails.wacStc = wacSurfaceTypeCost(x.surface);
            currentOsmDetails.wacSlopeCost = wacSlopeCost(x.incline);
            currentOsmDetails.wacWidthCost = wacWidthCost(x.width);
            currentOsmDetails.wacWidthRelevance = wacWidthRelevanceSwitch(x.width);
            currentOsmDetails.wacInaccessKerb = wacInaccessibleKerbCount(x.kerb);
            currentOsmDetails.wacCrossingCount = wacCrossingCount(x.crossing);
          }
        }

        // Compute current WAC
        let curWaclslswls = wacLSlopeLimitWidthLimit(5, currentOsmDetails.sIncline, currentOsmDetails.sWidth);
        let curWacwdnl = normalizedLengthWeighted(0.17, currentOsmDistance, 680);
        let curWacwtnlstc = normalizedSurfaceTypeCost(0.19, currentOsmDetails.wacStc, currentOsmDistance, 680);
        let curWacwsnlslc = normalizedSlopeCost(0.14, currentOsmDetails.wacSlopeCost, currentOsmDistance, 680);
        let curWacwwnlwcwrs = normalizedWidthCostWidthRelevance(0.09, currentOsmDetails.wacWidthCost, currentOsmDetails.wacWidthRelevance, currentOsmDistance, 680);
        let curWacwkikcck = wacInaccessKerbScore(0.26, currentOsmDetails.wacInaccessKerb, 680);
        let curWacwccrccc = wacCrossingScore(0.15, currentOsmDetails.wacCrossingCount, currentOsmDistance, 680);

        // Parse and Transform Data
        wacDataSetList.add({
          osmid: currentOsmDetails.osmid,
          wac: wacComputeScore(curWaclslswls, curWacwdnl, curWacwtnlstc, curWacwsnlslc, curWacwwnlwcwrs, curWacwkikcck, curWacwccrccc),
          l: 5,
          sls: currentOsmDetails.sIncline,
          wls: currentOsmDetails.sWidth,
          nl: normalizedLength(currentOsmDistance, 680),
          wdnl: normalizedLengthWeighted(0.17, currentOsmDistance, 680),
          lslswls: wacLSlopeLimitWidthLimit(5, currentOsmDetails.sIncline, currentOsmDetails.sWidth),
          stc: currentOsmDetails.wacStc,
          wtnlstc: normalizedSurfaceTypeCost(0.19, currentOsmDetails.wacStc, currentOsmDistance, 680),
          slc: currentOsmDetails.wacSlopeCost,
          wsnlslc: normalizedSlopeCost(0.14, currentOsmDetails.wacSlopeCost, currentOsmDistance, 680),
          wc: currentOsmDetails.wacWidthCost,
          wrs: currentOsmDetails.wacWidthRelevance,
          wwnlwcwrs: normalizedWidthCostWidthRelevance(0.09, currentOsmDetails.wacWidthCost, currentOsmDetails.wacWidthRelevance, currentOsmDistance, 680),
          ikc: currentOsmDetails.wacInaccessKerb,
          ck: ckScore(680),
          wkikcck: wacInaccessKerbScore(0.26, currentOsmDetails.wacInaccessKerb, 680),
          crc: currentOsmDetails.wacCrossingCount,
          cc: ccScore(680),
          wccrccc: wacCrossingScore(0.15, currentOsmDetails.wacCrossingCount, currentOsmDistance, 680),
          wayorder: currentWayOrder
        });

        // For Email Samples
        rawWacAnalysisSetList.add({
          osmid: currentOsmDetails.osmid,
          wac: wacComputeScore(curWaclslswls, curWacwdnl, curWacwtnlstc, curWacwsnlslc, curWacwwnlwcwrs, curWacwkikcck, curWacwccrccc),
          l: 5,
          kerbtype: currentOsmDetails.kerb,
          ikc: currentOsmDetails.wacInaccessKerb,
          slope: currentOsmDetails.incline,
          surfacetype: currentOsmDetails.surface,
          stc: currentOsmDetails.wacStc,
          slc: currentOsmDetails.wacSlopeCost,
          width: currentOsmDetails.width,
          wrs: currentOsmDetails.wacWidthRelevance,
          wc: currentOsmDetails.wacWidthCost,
          crossingtype: currentOsmDetails.crossing,
          crc: currentOsmDetails.wacCrossingCount,
          wsnlslc: normalizedSlopeCost(0.14, currentOsmDetails.wacSlopeCost, currentOsmDistance, 680),
          crossingtype: currentOsmDetails.crossing,
          kerbtype: currentOsmDetails.kerb,
          lslswls: wacLSlopeLimitWidthLimit(5, currentOsmDetails.sIncline, currentOsmDetails.sWidth),
          wdnl: normalizedLengthWeighted(0.17, currentOsmDistance, 680),
          wtnlstc: normalizedSurfaceTypeCost(0.19, currentOsmDetails.wacStc, currentOsmDistance, 680),
          wwnlwcwrs: normalizedWidthCostWidthRelevance(0.09, currentOsmDetails.wacWidthCost, currentOsmDetails.wacWidthRelevance, currentOsmDistance, 680),
          wkikcck: wacInaccessKerbScore(0.26, currentOsmDetails.wacInaccessKerb, 680),
          wccrccc: wacCrossingScore(0.15, currentOsmDetails.wacCrossingCount, currentOsmDistance, 680),
          distance: currentOsmDistance,
          segmentmaxlength: 680,
          normalizedlength: normalizedLength(currentOsmDistance, 680),
          startlongtitude: [...coordinatesSetList][nodeStartIndex].longtitude,
          startlatitude: [...coordinatesSetList][nodeStartIndex].latitude,
          endlongtitude: [...coordinatesSetList][nodeEndIndex].longtitude,
          endlatitude: [...coordinatesSetList][nodeEndIndex].latitude,
          wayorder: currentWayOrder
        });
      });

      // Total the Values of the WAC Data
      let finalWacScore = 0;
      wacDataSetList.forEach(el => {
        finalWacScore = finalWacScore + wacComputeScore(el.lslswls, el.wdnl, el.wtnlstc, el.wsnlslc, el.wwnlwcwrs, el.wkikcck, el.wccrccc);
      });
      let cleanFinalScore = parseFloat(finalWacScore.toFixed(6));
      // console.log("Creating Wheelchair Accessibility Cost Table");
      commit('SET_WAC_UNCORRECTED_ANALYSIS', [...wacDataSetList]);
      commit('SET_RAW_WAC_UNCORRECTED_DATA', [...rawWacAnalysisSetList]);
      commit('SET_UNCOR_ROUTING_NODE_BUCKET', [...lineMarkerSetList]);
      commit('SET_WAC_UNCORRECTED_SCORE', cleanFinalScore);
    } catch (err) {
      console.log(err);
    }
  },

  // Corrected ORS Server Processing
  async PROCESS_LIVE_PROFILE_ROUTE({ commit }, { markerList, profileChoice, avoidPolygon = [], maximum_incline = "20", maximum_sloped_kerb = "5", minimum_width = 0.815, surface_type = "cobblestone", routingType = "recommended" }) {

    let liv_rec_raw;
    // Storage of Way IDS    
    let liv_rec_wayids = [];

    // Storage of Tags Attributes 
    let storage_tags_attr = [];

    // Storage of Routing Coordinates
    let storage_routingCoord = [];

    // Storage of OSM Way Query;  
    let liv_rec_wayQuery = "";

    // Google Direction
    let gcp_raw_payload;

    //  Create a Segment Location Map    
    //  this set will contain the way index of the osmid
    const nodeSegmentMap = new Set();
    const elevationSetList = new Set();
    const coordinatesSetList = new Set();
    const rawWacSetList = new Set();
    const lineMarkerSetList = new Set();
    const osmDistanceSetList = new Set();
    const wacDataSetList = new Set();
    const rawWacAnalysisSetList = new Set();

    // Restriction Preference of User
    let myRoutingOption = {
      profile_params: {
        restrictions: {
          maximum_incline: maximum_incline,
          maximum_sloped_kerb: maximum_sloped_kerb,
          minimum_width: minimum_width,
          surface_type: surface_type
        },
        surface_quality_known: false,
        allow_unsuitable: false
      },
      avoid_polygons: {
        type: "MultiPolygon",
        coordinates: avoidPolygon
      }
    };

    if (avoidPolygon.length == 0) {
      delete myRoutingOption['avoid_polygons'];
    }
    if (maximum_sloped_kerb == 0) {
      delete myRoutingOption.profile_params.restrictions.maximum_sloped_kerb;
    }
    if (maximum_incline == 0) {
      delete myRoutingOption.profile_params.restrictions.maximum_incline;
    }
    if (profileChoice != 'wheelchair') {
      delete myRoutingOption['profile_params'];
    }

    try {
      liv_rec_raw = await axios.post(`${this.$config.CORRECTED_ORS_ENDPOINT}/v2/directions/${profileChoice}/geojson`, {
        coordinates: markerList,
        extra_info: ["osmid", "waytype", "steepness"],
        elevation: true,
        instructions_format: "text",
        language: "en",
        preference: routingType,
        options: myRoutingOption
      }
      );
      if (liv_rec_raw) {
        // prettier-ignore
        for (let itr = 0; itr < liv_rec_raw.data.features[0].geometry.coordinates.length; itr++) {
          coordinatesSetList.add({ id: itr, latitude: liv_rec_raw.data.features[0].geometry.coordinates[itr][1], longtitude: liv_rec_raw.data.features[0].geometry.coordinates[itr][0] });
          storage_routingCoord.push([liv_rec_raw.data.features[0].geometry.coordinates[itr][1], liv_rec_raw.data.features[0].geometry.coordinates[itr][0]]);
          elevationSetList.add(liv_rec_raw.data.features[0].geometry.coordinates[itr][2]);

        }
        // prettier-ignore
        if (liv_rec_raw.data.features[0].properties.extras.osmId) {
          for (let i = 0; i < liv_rec_raw.data.features[0].properties.extras.osmId.values.length; i++) {
            // Place the Segment in the list to map array
            osmDistanceSetList.add({ osmid: liv_rec_raw.data.features[0].properties.extras.osmId.summary[i].value, distance: liv_rec_raw.data.features[0].properties.extras.osmId.summary[i].distance, amount: liv_rec_raw.data.features[0].properties.extras.osmId.summary[i].amount, wayorder: liv_rec_raw.data.features[0].properties.extras.osmId.values[i][0] });
            nodeSegmentMap.add({ osmid: liv_rec_raw.data.features[0].properties.extras.osmId.values[i][2], startIndex: liv_rec_raw.data.features[0].properties.extras.osmId.values[i][0], endIndex: liv_rec_raw.data.features[0].properties.extras.osmId.values[i][1] });
            let myArr = liv_rec_raw.data.features[0].properties.extras.osmId.values[i];
            liv_rec_wayids.push(myArr.slice(-1).pop());
          }
          for (let iter in liv_rec_wayids) {
            liv_rec_wayQuery = liv_rec_wayQuery + "way(" + liv_rec_wayids[iter] + ");";
          }
          let myTagResult = await axios.get(`${this.$config.LOCAL_OVERPASS_ENDPOINT}/api/interpreter?data=[out:json];(${liv_rec_wayQuery});out%20tags;`);
          if (myTagResult) {
            Object.assign(storage_tags_attr, myTagResult.data.elements);
            commit("SET_REC_LIV_FEATURE_LIST", storage_tags_attr);
          }
        } else {
          console.log("No OSM ID to be parsed.");
        }
      }
    } catch (err) {
      console.log("[PROCESS_LIVE_PROFILE_ROUTE] Error in Generating Recommended Route");
      console.log(err);
    }
    // prettier-ignore
    commit("SET_REC_LIV_RAWDATA", liv_rec_raw.data);
    commit("SET_REC_LIV_ROUTING_COORDINATES", storage_routingCoord);
    commit("SET_REC_LIV_WAYNODE_LIST", liv_rec_wayids);
    commit("SET_REC_LIV_ROUTE_INSTRUCTION", liv_rec_raw.data.features[0].properties.segments[0].steps);
    commit("SET_REC_LIV_ROUTE_ELEVATION", [...elevationSetList]);


    // This will use the google routing and needs a valid api key
    try {
      console.log("Starting GCP Directions Request");
      gcp_raw_payload = await axios.post(`${this.$config.GCP_DIRECTION_ENDPOINT}/api/generate`, {
        "origin": markerList[0][1] + ',' + markerList[0][0],
        "destination": markerList[1][1] + ',' + markerList[1][0]
      });
      if (gcp_raw_payload) {
        // console.log("Finished GCP Directions Request");
        let decodedGcpPolyline = decode(gcp_raw_payload.data.routes[0].overview_polyline.points);
        commit("SET_GCP_RAWDATA", gcp_raw_payload.data);
        commit("SET_GCP_ROUTING_COORDINATES", decodedGcpPolyline);
      }
    } catch (err) {
      console.log("Error in Generating Google Route");
      console.log(err);
    }

    try {

      // Counters for the Statistics
      let tagCounter = _.size(storage_tags_attr);

      for (let itr = 0; itr < tagCounter; ++itr) {
        rawWacSetList.add({
          osmid: storage_tags_attr[itr].id,
          coordinates: storage_routingCoord[itr],
          surface: checkSurfaceType(storage_tags_attr[itr].tags.surface),
          highway: storage_tags_attr[itr].tags.highway,
          lit: checkLitValue(storage_tags_attr[itr].tags.lit),
          crossing: checkCrossingType(storage_tags_attr[itr].tags.crossing),
          incline: checkInclineValue(storage_tags_attr[itr].tags.incline),
          kerb: checkKerbType(storage_tags_attr[itr].tags.kerb),
          width: widthFormatter(storage_tags_attr[itr].tags.width)
        });
      }
      rawWacSetList.forEach(el => {
        // Search in the Segment osm  ID and assign lat long and assign the corresponding order
        let nodeStartIndex;
        let nodeEndIndex;
        let currentWayOrder;
        for (let e of nodeSegmentMap) {
          if (e.osmid === el.osmid) {
            nodeStartIndex = e.startIndex;
            nodeEndIndex = e.endIndex;
            currentWayOrder = e.startIndex;
            break;
          }
        }

        // Add the lat-lng value of the markers
        lineMarkerSetList.add([[...coordinatesSetList][nodeStartIndex].latitude, [...coordinatesSetList][nodeStartIndex].longtitude]);
        lineMarkerSetList.add([[...coordinatesSetList][nodeEndIndex].latitude, [...coordinatesSetList][nodeEndIndex].longtitude]);

        //  Search in Distance Set List the osmid and assign the corresponding distance of segment
        let currentOsmDistance;
        for (let d of osmDistanceSetList) {
          if (d.osmid === el.osmid) {
            currentOsmDistance = d.distance;
            break;
          }
        }

        // Set Data in the Object
        let currentOsmDetails = {};
        for (let x of rawWacSetList) {
          if (x.osmid === el.osmid) {
            currentOsmDetails.osmid = x.osmid;
            currentOsmDetails.surface = x.surface;
            currentOsmDetails.highway = x.highway;
            currentOsmDetails.lit = x.lit;
            currentOsmDetails.crossing = x.crossing;
            currentOsmDetails.incline = x.incline;
            currentOsmDetails.kerb = x.kerb;
            currentOsmDetails.width = x.width;
            currentOsmDetails.sWidth = wacWidthLimitSwitch(x.width);
            currentOsmDetails.sIncline = wacSlopeLimitSwitch(x.incline);
            currentOsmDetails.wacStc = wacSurfaceTypeCost(x.surface);
            currentOsmDetails.wacSlopeCost = wacSlopeCost(x.incline);
            currentOsmDetails.wacWidthCost = wacWidthCost(x.width);
            currentOsmDetails.wacWidthRelevance = wacWidthRelevanceSwitch(x.width);
            currentOsmDetails.wacInaccessKerb = wacInaccessibleKerbCount(x.kerb);
            currentOsmDetails.wacCrossingCount = wacCrossingCount(x.crossing);
          }
        }

        // Compute current WAC
        let curWaclslswls = wacLSlopeLimitWidthLimit(5, currentOsmDetails.sIncline, currentOsmDetails.sWidth);
        let curWacwdnl = normalizedLengthWeighted(0.17, currentOsmDistance, 680);
        let curWacwtnlstc = normalizedSurfaceTypeCost(0.19, currentOsmDetails.wacStc, currentOsmDistance, 680);
        let curWacwsnlslc = normalizedSlopeCost(0.14, currentOsmDetails.wacSlopeCost, currentOsmDistance, 680);
        let curWacwwnlwcwrs = normalizedWidthCostWidthRelevance(0.09, currentOsmDetails.wacWidthCost, currentOsmDetails.wacWidthRelevance, currentOsmDistance, 680);
        let curWacwkikcck = wacInaccessKerbScore(0.26, currentOsmDetails.wacInaccessKerb, 680);
        let curWacwccrccc = wacCrossingScore(0.15, currentOsmDetails.wacCrossingCount, currentOsmDistance, 680);

        // Parse and Transform Data
        wacDataSetList.add({
          osmid: currentOsmDetails.osmid,
          wac: wacComputeScore(curWaclslswls, curWacwdnl, curWacwtnlstc, curWacwsnlslc, curWacwwnlwcwrs, curWacwkikcck, curWacwccrccc),
          l: 5,
          sls: currentOsmDetails.sIncline,
          wls: currentOsmDetails.sWidth,
          nl: normalizedLength(currentOsmDistance, 680),
          wdnl: normalizedLengthWeighted(0.17, currentOsmDistance, 680),
          lslswls: wacLSlopeLimitWidthLimit(5, currentOsmDetails.sIncline, currentOsmDetails.sWidth),
          stc: currentOsmDetails.wacStc,
          wtnlstc: normalizedSurfaceTypeCost(0.19, currentOsmDetails.wacStc, currentOsmDistance, 680),
          slc: currentOsmDetails.wacSlopeCost,
          wsnlslc: normalizedSlopeCost(0.14, currentOsmDetails.wacSlopeCost, currentOsmDistance, 680),
          wc: currentOsmDetails.wacWidthCost,
          wrs: currentOsmDetails.wacWidthRelevance,
          wwnlwcwrs: normalizedWidthCostWidthRelevance(0.09, currentOsmDetails.wacWidthCost, currentOsmDetails.wacWidthRelevance, currentOsmDistance, 680),
          ikc: currentOsmDetails.wacInaccessKerb,
          ck: ckScore(680),
          wkikcck: wacInaccessKerbScore(0.26, currentOsmDetails.wacInaccessKerb, 680),
          crc: currentOsmDetails.wacCrossingCount,
          cc: ccScore(680),
          wccrccc: wacCrossingScore(0.15, currentOsmDetails.wacCrossingCount, currentOsmDistance, 680),
          wayorder: currentWayOrder
        });



        // For Email Samples
        rawWacAnalysisSetList.add({
          osmid: currentOsmDetails.osmid,
          wac: wacComputeScore(curWaclslswls, curWacwdnl, curWacwtnlstc, curWacwsnlslc, curWacwwnlwcwrs, curWacwkikcck, curWacwccrccc),
          l: 5,
          kerbtype: currentOsmDetails.kerb,
          ikc: currentOsmDetails.wacInaccessKerb,
          slope: currentOsmDetails.incline,
          surfacetype: currentOsmDetails.surface,
          stc: currentOsmDetails.wacStc,
          slc: currentOsmDetails.wacSlopeCost,
          width: currentOsmDetails.width,
          wrs: currentOsmDetails.wacWidthRelevance,
          wc: currentOsmDetails.wacWidthCost,
          crossingtype: currentOsmDetails.crossing,
          crc: currentOsmDetails.wacCrossingCount,
          wsnlslc: normalizedSlopeCost(0.14, currentOsmDetails.wacSlopeCost, currentOsmDistance, 680),
          crossingtype: currentOsmDetails.crossing,
          kerbtype: currentOsmDetails.kerb,
          lslswls: wacLSlopeLimitWidthLimit(5, currentOsmDetails.sIncline, currentOsmDetails.sWidth),
          wdnl: normalizedLengthWeighted(0.17, currentOsmDistance, 680),
          wtnlstc: normalizedSurfaceTypeCost(0.19, currentOsmDetails.wacStc, currentOsmDistance, 680),
          wwnlwcwrs: normalizedWidthCostWidthRelevance(0.09, currentOsmDetails.wacWidthCost, currentOsmDetails.wacWidthRelevance, currentOsmDistance, 680),
          wkikcck: wacInaccessKerbScore(0.26, currentOsmDetails.wacInaccessKerb, 680),
          wccrccc: wacCrossingScore(0.15, currentOsmDetails.wacCrossingCount, currentOsmDistance, 680),
          distance: currentOsmDistance,
          segmentmaxlength: 680,
          normalizedlength: normalizedLength(currentOsmDistance, 680),
          startlongtitude: [...coordinatesSetList][nodeStartIndex].longtitude,
          startlatitude: [...coordinatesSetList][nodeStartIndex].latitude,
          endlongtitude: [...coordinatesSetList][nodeEndIndex].longtitude,
          endlatitude: [...coordinatesSetList][nodeEndIndex].latitude,
          wayorder: currentWayOrder
        });
      });

      // Total the Values of the WAC Data
      let finalWacScore = 0;
      wacDataSetList.forEach(el => {
        finalWacScore = finalWacScore + wacComputeScore(el.lslswls, el.wdnl, el.wtnlstc, el.wsnlslc, el.wwnlwcwrs, el.wkikcck, el.wccrccc);
      });
      let cleanFinalScore = parseFloat(finalWacScore.toFixed(6));

      // prettier-ignore
      commit('SET_WAC_CORRECTED_ANALYSIS', [...wacDataSetList]);
      commit('SET_RAW_WAC_CORRECTED_DATA', [...rawWacAnalysisSetList]);
      commit('SET_WAC_CORRECTED_SCORE', cleanFinalScore);
      commit('SET_COR_ROUTING_NODE_BUCKET', [...lineMarkerSetList]);
    } catch (err) {
      console.log(err);
    }
  },

  // No Attribute ORS Server Processing
  async PROCESS_NOATTR_ROUTING({ commit }, { markerList, profileChoice, avoidPolygon = [], routingType = "recommended" }) {

    let noAttrRawData;
    // Storage of Way IDS
    let osm_wayid_query = [];

    // Storage of Tags Attributes
    let storage_tags_attr = [];

    // Storage of Routing Coordinates
    let storage_routingCoord = [];

    // Storage of OSM Way Query;
    let noattr_wayQuery = "";

    //  Create a Segment Location Map    
    //  this set will contain the way index of the osmid
    const nodeSegmentMap = new Set();
    const elevationSetList = new Set();
    const coordinatesSetList = new Set();
    const rawWacSetList = new Set();
    const lineMarkerSetList = new Set();
    const osmDistanceSetList = new Set();
    const wacDataSetList = new Set();
    const rawWacAnalysisSetList = new Set();

    // Restriction Preference of User
    let myRoutingOption = {
      profile_params: {
        restrictions: {
        },
        surface_quality_known: false,
        allow_unsuitable: false
      },
      avoid_polygons: {
        type: "MultiPolygon",
        coordinates: avoidPolygon
      }
    };



    if (profileChoice != 'wheelchair') {
      delete myRoutingOption['profile_params'];
    }
    try {
      noAttrRawData = await axios.post(`${this.$config.NOATTRIBUTE_ORS_ENDPOINT}/v2/directions/${profileChoice}/geojson`, {
        coordinates: markerList,
        extra_info: ["osmid", "waytype", "steepness"],
        elevation: true,
        instructions_format: "text",
        language: "en",
        preference: routingType,
        options: myRoutingOption
      }
      );
      if (noAttrRawData) {
        // prettier-ignore
        for (let itr = 0; itr < noAttrRawData.data.features[0].geometry.coordinates.length; itr++) {
          coordinatesSetList.add({ id: itr, latitude: noAttrRawData.data.features[0].geometry.coordinates[itr][1], longtitude: noAttrRawData.data.features[0].geometry.coordinates[itr][0] });
          storage_routingCoord.push([noAttrRawData.data.features[0].geometry.coordinates[itr][1], noAttrRawData.data.features[0].geometry.coordinates[itr][0]]);
          elevationSetList.add(noAttrRawData.data.features[0].geometry.coordinates[itr][2]);
        }
        // prettier-ignore
        if (noAttrRawData.data.features[0].properties.extras.osmId) {
          const checkOsmIdSize = _.size(noAttrRawData.data.features[0].properties.extras.osmId.values);

          for (let i = 0; i < checkOsmIdSize; i++) {
            osmDistanceSetList.add({ osmid: noAttrRawData.data.features[0].properties.extras.osmId.summary[i].value, distance: noAttrRawData.data.features[0].properties.extras.osmId.summary[i].distance, amount: noAttrRawData.data.features[0].properties.extras.osmId.summary[i].amount, wayorder: noAttrRawData.data.features[0].properties.extras.osmId.values[i][0] });
            // Place the Segment in the list to map array
            nodeSegmentMap.add({ osmid: noAttrRawData.data.features[0].properties.extras.osmId.values[i][2], startIndex: noAttrRawData.data.features[0].properties.extras.osmId.values[i][0], endIndex: noAttrRawData.data.features[0].properties.extras.osmId.values[i][1] });
            let myArr = noAttrRawData.data.features[0].properties.extras.osmId.values[i];
            osm_wayid_query.push(myArr.slice(-1).pop());
          }
          for (let iter in osm_wayid_query) {
            noattr_wayQuery = noattr_wayQuery + "way(" + osm_wayid_query[iter] + ");";
          }
          try {
            let myTagResult = await axios.get(`${this.$config.LOCAL_OVERPASS_ENDPOINT}/api/interpreter?data=[out:json];(${noattr_wayQuery});out%20tags;`);
            if (myTagResult) {
              Object.assign(storage_tags_attr, myTagResult.data.elements);
              commit("SET_NOATTR_FEATURE_LIST", storage_tags_attr);
            }
          } catch (err) {
            console.log("[PROCESS_NOATTR_ROUTING] Error in Requesting Overpass Way Query.");
          }

        } else {
          console.log("[PROCESS_NOATTR_ROUTING] No OSM ID to be parsed.");
        }

      }
    } catch (err) {
      console.log("[PROCESS_NOATTR_ROUTING] Error in Generating Route Tags");
      console.log(err);
    }

    // prettier-ignore
    commit("SET_NOATTR_RAWDATA", noAttrRawData.data);
    commit("SET_NOATTR_ROUTING_COORDINATES", storage_routingCoord);
    commit("SET_NOATTR_WAYNODE_LIST", osm_wayid_query);
    commit("SET_NOATTR_ROUTE_INSTRUCTION", noAttrRawData.data.features[0].properties.segments[0].steps);
    commit("SET_NOATTR_ROUTE_ELEVATION", [...elevationSetList]);

    try {
      // Counters for the Statistics
      let tagCounter = _.size(storage_tags_attr);

      for (let itr = 0; itr < tagCounter; ++itr) {
        rawWacSetList.add({
          osmid: storage_tags_attr[itr].id,
          coordinates: storage_routingCoord[itr],
          surface: checkSurfaceType(storage_tags_attr[itr].tags.surface),
          highway: storage_tags_attr[itr].tags.highway,
          lit: checkLitValue(storage_tags_attr[itr].tags.lit),
          crossing: checkCrossingType(storage_tags_attr[itr].tags.crossing),
          incline: checkInclineValue(storage_tags_attr[itr].tags.incline),
          kerb: checkKerbType(storage_tags_attr[itr].tags.kerb),
          width: widthFormatter(storage_tags_attr[itr].tags.width),
        });
      }


      rawWacSetList.forEach(el => {
        // Search in the Segment osm  ID and assign lat long and assign the corresponding order
        let nodeStartIndex;
        let nodeEndIndex;
        let currentWayOrder;
        for (let e of nodeSegmentMap) {
          if (e.osmid === el.osmid) {
            nodeStartIndex = e.startIndex;
            nodeEndIndex = e.endIndex;
            currentWayOrder = e.startIndex;
            break;
          }
        }

        // Add the lat-lng value of the markers
        lineMarkerSetList.add([[...coordinatesSetList][nodeStartIndex].latitude, [...coordinatesSetList][nodeStartIndex].longtitude]);
        lineMarkerSetList.add([[...coordinatesSetList][nodeEndIndex].latitude, [...coordinatesSetList][nodeEndIndex].longtitude]);

        //  Search in Distance Set List the osmid and assign the corresponding distance of segment
        let currentOsmDistance;
        for (let d of osmDistanceSetList) {
          if (d.osmid === el.osmid) {
            currentOsmDistance = d.distance;
            break;
          }
        }

        // Set Data in the Object
        let currentOsmDetails = {};
        for (let x of rawWacSetList) {
          if (x.osmid === el.osmid) {
            currentOsmDetails.osmid = x.osmid;
            currentOsmDetails.surface = x.surface;
            currentOsmDetails.highway = x.highway;
            currentOsmDetails.lit = x.lit;
            currentOsmDetails.crossing = x.crossing;
            currentOsmDetails.incline = x.incline;
            currentOsmDetails.kerb = x.kerb;
            currentOsmDetails.width = x.width;
            currentOsmDetails.sWidth = wacWidthLimitSwitch(x.width);
            currentOsmDetails.sIncline = wacSlopeLimitSwitch(x.incline);
            currentOsmDetails.wacStc = wacSurfaceTypeCost(x.surface);
            currentOsmDetails.wacSlopeCost = wacSlopeCost(x.incline);
            currentOsmDetails.wacWidthCost = wacWidthCost(x.width);
            currentOsmDetails.wacWidthRelevance = wacWidthRelevanceSwitch(x.width);
            currentOsmDetails.wacInaccessKerb = wacInaccessibleKerbCount(x.kerb);
            currentOsmDetails.wacCrossingCount = wacCrossingCount(x.crossing);
          }
        }

        // Compute current WAC
        let curWaclslswls = wacLSlopeLimitWidthLimit(5, currentOsmDetails.sIncline, currentOsmDetails.sWidth);
        let curWacwdnl = normalizedLengthWeighted(0.17, currentOsmDistance, 680);
        let curWacwtnlstc = normalizedSurfaceTypeCost(0.19, currentOsmDetails.wacStc, currentOsmDistance, 680);
        let curWacwsnlslc = normalizedSlopeCost(0.14, currentOsmDetails.wacSlopeCost, currentOsmDistance, 680);
        let curWacwwnlwcwrs = normalizedWidthCostWidthRelevance(0.09, currentOsmDetails.wacWidthCost, currentOsmDetails.wacWidthRelevance, currentOsmDistance, 680);
        let curWacwkikcck = wacInaccessKerbScore(0.26, currentOsmDetails.wacInaccessKerb, 680);
        let curWacwccrccc = wacCrossingScore(0.15, currentOsmDetails.wacCrossingCount, currentOsmDistance, 680);


        // Parse and Transform Data
        wacDataSetList.add({
          osmid: currentOsmDetails.osmid,
          wac: wacComputeScore(curWaclslswls, curWacwdnl, curWacwtnlstc, curWacwsnlslc, curWacwwnlwcwrs, curWacwkikcck, curWacwccrccc),
          l: 5,
          sls: currentOsmDetails.sIncline,
          wls: currentOsmDetails.sWidth,
          nl: normalizedLength(currentOsmDistance, 680),
          wdnl: normalizedLengthWeighted(0.17, currentOsmDistance, 680),
          lslswls: wacLSlopeLimitWidthLimit(5, currentOsmDetails.sIncline, currentOsmDetails.sWidth),
          stc: currentOsmDetails.wacStc,
          wtnlstc: normalizedSurfaceTypeCost(0.19, currentOsmDetails.wacStc, currentOsmDistance, 680),
          slc: currentOsmDetails.wacSlopeCost,
          wsnlslc: normalizedSlopeCost(0.14, currentOsmDetails.wacSlopeCost, currentOsmDistance, 680),
          wc: currentOsmDetails.wacWidthCost,
          wrs: currentOsmDetails.wacWidthRelevance,
          wwnlwcwrs: normalizedWidthCostWidthRelevance(0.09, currentOsmDetails.wacWidthCost, currentOsmDetails.wacWidthRelevance, currentOsmDistance, 680),
          ikc: currentOsmDetails.wacInaccessKerb,
          ck: ckScore(680),
          wkikcck: wacInaccessKerbScore(0.26, currentOsmDetails.wacInaccessKerb, 680),
          crc: currentOsmDetails.wacCrossingCount,
          cc: ccScore(680),
          wccrccc: wacCrossingScore(0.15, currentOsmDetails.wacCrossingCount, currentOsmDistance, 680),
          wayorder: currentWayOrder
        });




        // For Email Samples
        rawWacAnalysisSetList.add({
          osmid: currentOsmDetails.osmid,
          wac: wacComputeScore(curWaclslswls, curWacwdnl, curWacwtnlstc, curWacwsnlslc, curWacwwnlwcwrs, curWacwkikcck, curWacwccrccc),
          l: 5,
          kerbtype: currentOsmDetails.kerb,
          ikc: currentOsmDetails.wacInaccessKerb,
          slope: currentOsmDetails.incline,
          surfacetype: currentOsmDetails.surface,
          stc: currentOsmDetails.wacStc,
          slc: currentOsmDetails.wacSlopeCost,
          width: currentOsmDetails.width,
          wrs: currentOsmDetails.wacWidthRelevance,
          wc: currentOsmDetails.wacWidthCost,
          crossingtype: currentOsmDetails.crossing,
          crc: currentOsmDetails.wacCrossingCount,
          wsnlslc: normalizedSlopeCost(0.14, currentOsmDetails.wacSlopeCost, currentOsmDistance, 680),
          crossingtype: currentOsmDetails.crossing,
          kerbtype: currentOsmDetails.kerb,
          lslswls: wacLSlopeLimitWidthLimit(5, currentOsmDetails.sIncline, currentOsmDetails.sWidth),
          wdnl: normalizedLengthWeighted(0.17, currentOsmDistance, 680),
          wtnlstc: normalizedSurfaceTypeCost(0.19, currentOsmDetails.wacStc, currentOsmDistance, 680),
          wwnlwcwrs: normalizedWidthCostWidthRelevance(0.09, currentOsmDetails.wacWidthCost, currentOsmDetails.wacWidthRelevance, currentOsmDistance, 680),
          wkikcck: wacInaccessKerbScore(0.26, currentOsmDetails.wacInaccessKerb, 680),
          wccrccc: wacCrossingScore(0.15, currentOsmDetails.wacCrossingCount, currentOsmDistance, 680),
          distance: currentOsmDistance,
          segmentmaxlength: 680,
          normalizedlength: normalizedLength(currentOsmDistance, 680),
          startlongtitude: [...coordinatesSetList][nodeStartIndex].longtitude,
          startlatitude: [...coordinatesSetList][nodeStartIndex].latitude,
          endlongtitude: [...coordinatesSetList][nodeEndIndex].longtitude,
          endlatitude: [...coordinatesSetList][nodeEndIndex].latitude,
          wayorder: currentWayOrder
        });
      });

      // Total the Values of the WAC Data
      let finalWacScore = 0;
      wacDataSetList.forEach(el => {
        finalWacScore = finalWacScore + wacComputeScore(el.lslswls, el.wdnl, el.wtnlstc, el.wsnlslc, el.wwnlwcwrs, el.wkikcck, el.wccrccc);
      });
      let cleanFinalScore = parseFloat(finalWacScore.toFixed(6));

      commit('SET_WAC_NOATTR_SCORE', cleanFinalScore);
      commit('SET_WAC_NOATTR_ANALYSIS', [...wacDataSetList]);
      commit('SET_RAW_WAC_NOATTR_DATA', [...rawWacAnalysisSetList]);
      commit('SET_NOATTR_ROUTING_NODE_BUCKET', [...lineMarkerSetList]);
    } catch (err) {
      console.log("[PROCESS_NOATTR_ROUTING] Error in Generating Route");
      console.log(err);
    }
  },

  // This function will use the uncorrected ORS Endpoing and analysis data will be going to the uncorrected analysis and evaluation uncorrected
  async PROCESS_SIMULATION_DATA_UNCORRECTED({ commit }, { analysisid = "", profileChoice = "wheelchair", origin, destination, maximum_incline = "20", minimum_width = 0.815, surface_type = "cobblestone", avoidPolygon = [] }) {

    let simulation_rawdata;

    // Storage of Way IDS
    let osm_wayid_query = [];

    // Storage of Tags Attributes
    let storage_tags_attr = [];

    // Storage of Routing Coordinates
    let storage_routingCoord = [];

    // Storage of OSM Way Query;
    let osmWayQuery = "";

    //  Create a Segment Location Map    
    //  this set will contain the way index of the osmid
    const nodeSegmentMap = new Set();
    const elevationSetList = new Set();
    const coordinatesSetList = new Set();
    const rawWacSetList = new Set();
    const lineMarkerSetList = new Set();
    const osmDistanceSetList = new Set();
    const wacDataSetList = new Set();
    const rawWacAnalysisSetList = new Set();

    // Routing Options
    let myRoutingOption = {
      profile_params: {
        restrictions: {
          maximum_incline: maximum_incline,
          minimum_width: minimum_width,
          surface_type: surface_type
        },
        surface_quality_known: false,
        allow_unsuitable: false
      },
      avoid_polygons: {
        type: "MultiPolygon",
        coordinates: avoidPolygon
      }
    };

    // Routing Parameter Check
    if (avoidPolygon.length == 0) {
      delete myRoutingOption['avoid_polygons'];
    }
    if (maximum_incline == 0) {
      delete myRoutingOption.profile_params.restrictions.maximum_incline;
    }
    if (profileChoice != 'wheelchair') {
      delete myRoutingOption['profile_params'];
    }

    // Run Request
    try {
      simulation_rawdata = await axios.post(`${this.$config.UNCORRECTED_ORS_ENDPOINT}/v2/directions/${profileChoice}/geojson`, {
        coordinates: [[origin[1], origin[0]], [destination[1], destination[0]]],
        extra_info: ["osmid", "waytype", "steepness"],
        elevation: true,
        preference: "recommended",
        options: myRoutingOption
      }
      );
      if (simulation_rawdata) {
        // prettier-ignore
        for (let itr = 0; itr < simulation_rawdata.data.features[0].geometry.coordinates.length; itr++) {
          coordinatesSetList.add({ id: itr, latitude: simulation_rawdata.data.features[0].geometry.coordinates[itr][1], longtitude: simulation_rawdata.data.features[0].geometry.coordinates[itr][0] });
          storage_routingCoord.push([simulation_rawdata.data.features[0].geometry.coordinates[itr][1], simulation_rawdata.data.features[0].geometry.coordinates[itr][0]]);
          elevationSetList.add(simulation_rawdata.data.features[0].geometry.coordinates[itr][2]);
        }
        // prettier-ignore
        if (simulation_rawdata.data.features[0].properties.extras.osmId) {
          for (let i = 0; i < simulation_rawdata.data.features[0].properties.extras.osmId.values.length; i++) {
            osmDistanceSetList.add({ osmid: simulation_rawdata.data.features[0].properties.extras.osmId.summary[i].value, distance: simulation_rawdata.data.features[0].properties.extras.osmId.summary[i].distance, amount: simulation_rawdata.data.features[0].properties.extras.osmId.summary[i].amount, wayorder: simulation_rawdata.data.features[0].properties.extras.osmId.values[i][0] });
            // Place the Segment in the list to map array
            nodeSegmentMap.add({ osmid: simulation_rawdata.data.features[0].properties.extras.osmId.values[i][2], startIndex: simulation_rawdata.data.features[0].properties.extras.osmId.values[i][0], endIndex: simulation_rawdata.data.features[0].properties.extras.osmId.values[i][1] });
            let myArr = simulation_rawdata.data.features[0].properties.extras.osmId.values[i];
            osm_wayid_query.push(myArr.slice(-1).pop());
          }
        } else {
          console.log("No OSM ID to be parsed.");
        }
        // Get OSM TAGS based on OSM ID from the Route Generated
        for (let iter in osm_wayid_query) {
          osmWayQuery = osmWayQuery + "way(" + osm_wayid_query[iter] + ");";
        }
        try {
          const myTagResult = await axios.get(`${this.$config.LOCAL_OVERPASS_ENDPOINT}/api/interpreter?data=[out:json];(${osmWayQuery});out%20tags;`);
          if (myTagResult) {
            Object.assign(storage_tags_attr, myTagResult.data.elements);
          }
        }
        catch (err) {
          console.log('Error in Getting Tag Result');
          console.log(err);
        }
      }

      try {
        // Counters for the Statistics
        let tagCounter = _.size(storage_tags_attr);

        for (let itr = 0; itr < tagCounter; ++itr) {
          rawWacSetList.add({
            osmid: storage_tags_attr[itr].id,
            coordinates: storage_routingCoord[itr],
            surface: checkSurfaceType(storage_tags_attr[itr].tags.surface),
            highway: storage_tags_attr[itr].tags.highway,
            lit: checkLitValue(storage_tags_attr[itr].tags.lit),
            crossing: checkCrossingType(storage_tags_attr[itr].tags.crossing),
            incline: checkInclineValue(storage_tags_attr[itr].tags.incline),
            kerb: checkKerbType(storage_tags_attr[itr].tags.kerb),
            width: widthFormatter(storage_tags_attr[itr].tags.width),
          });
        }

        rawWacSetList.forEach(el => {
          // Search in the Segment osm  ID and assign lat long and assign the corresponding order
          let nodeStartIndex;
          let nodeEndIndex;
          let currentWayOrder;
          for (let e of nodeSegmentMap) {
            if (e.osmid === el.osmid) {
              nodeStartIndex = e.startIndex;
              nodeEndIndex = e.endIndex;
              currentWayOrder = e.startIndex;
              break;
            }
          }

          // Add the lat-lng value of the markers
          lineMarkerSetList.add([[...coordinatesSetList][nodeStartIndex].latitude, [...coordinatesSetList][nodeStartIndex].longtitude]);
          lineMarkerSetList.add([[...coordinatesSetList][nodeEndIndex].latitude, [...coordinatesSetList][nodeEndIndex].longtitude]);

          //  Search in Distance Set List the osmid and assign the corresponding distance of segment
          let currentOsmDistance;
          for (let d of osmDistanceSetList) {
            if (d.osmid === el.osmid) {
              currentOsmDistance = d.distance;
              break;
            }
          }


          // Set Data in the Object
          let currentOsmDetails = {};
          for (let x of rawWacSetList) {
            if (x.osmid === el.osmid) {
              currentOsmDetails.osmid = x.osmid;
              currentOsmDetails.surface = x.surface;
              currentOsmDetails.highway = x.highway;
              currentOsmDetails.lit = x.lit;
              currentOsmDetails.crossing = x.crossing;
              currentOsmDetails.incline = x.incline;
              currentOsmDetails.kerb = x.kerb;
              currentOsmDetails.width = x.width;
              currentOsmDetails.sWidth = wacWidthLimitSwitch(x.width);
              currentOsmDetails.sIncline = wacSlopeLimitSwitch(x.incline);
              currentOsmDetails.wacStc = wacSurfaceTypeCost(x.surface);
              currentOsmDetails.wacSlopeCost = wacSlopeCost(x.incline);
              currentOsmDetails.wacWidthCost = wacWidthCost(x.width);
              currentOsmDetails.wacWidthRelevance = wacWidthRelevanceSwitch(x.width);
              currentOsmDetails.wacInaccessKerb = wacInaccessibleKerbCount(x.kerb);
              currentOsmDetails.wacCrossingCount = wacCrossingCount(x.crossing);
            }
          }

          // Compute current WAC
          let curWaclslswls = wacLSlopeLimitWidthLimit(5, currentOsmDetails.sIncline, currentOsmDetails.sWidth);
          let curWacwdnl = normalizedLengthWeighted(0.17, currentOsmDistance, 680);
          let curWacwtnlstc = normalizedSurfaceTypeCost(0.19, currentOsmDetails.wacStc, currentOsmDistance, 680);
          let curWacwsnlslc = normalizedSlopeCost(0.14, currentOsmDetails.wacSlopeCost, currentOsmDistance, 680);
          let curWacwwnlwcwrs = normalizedWidthCostWidthRelevance(0.09, currentOsmDetails.wacWidthCost, currentOsmDetails.wacWidthRelevance, currentOsmDistance, 680);
          let curWacwkikcck = wacInaccessKerbScore(0.26, currentOsmDetails.wacInaccessKerb, 680);
          let curWacwccrccc = wacCrossingScore(0.15, currentOsmDetails.wacCrossingCount, currentOsmDistance, 680);


          // Parse and Transform Data
          wacDataSetList.add({
            osmid: currentOsmDetails.osmid,
            wac: wacComputeScore(curWaclslswls, curWacwdnl, curWacwtnlstc, curWacwsnlslc, curWacwwnlwcwrs, curWacwkikcck, curWacwccrccc),
            l: 5,
            sls: currentOsmDetails.sIncline,
            wls: currentOsmDetails.sWidth,
            nl: normalizedLength(currentOsmDistance, 680),
            wdnl: normalizedLengthWeighted(0.17, currentOsmDistance, 680),
            lslswls: wacLSlopeLimitWidthLimit(5, currentOsmDetails.sIncline, currentOsmDetails.sWidth),
            stc: currentOsmDetails.wacStc,
            wtnlstc: normalizedSurfaceTypeCost(0.19, currentOsmDetails.wacStc, currentOsmDistance, 680),
            slc: currentOsmDetails.wacSlopeCost,
            wsnlslc: normalizedSlopeCost(0.14, currentOsmDetails.wacSlopeCost, currentOsmDistance, 680),
            wc: currentOsmDetails.wacWidthCost,
            wrs: currentOsmDetails.wacWidthRelevance,
            wwnlwcwrs: normalizedWidthCostWidthRelevance(0.09, currentOsmDetails.wacWidthCost, currentOsmDetails.wacWidthRelevance, currentOsmDistance, 680),
            ikc: currentOsmDetails.wacInaccessKerb,
            ck: ckScore(680),
            wkikcck: wacInaccessKerbScore(0.26, currentOsmDetails.wacInaccessKerb, 680),
            crc: currentOsmDetails.wacCrossingCount,
            cc: ccScore(680),
            wccrccc: wacCrossingScore(0.15, currentOsmDetails.wacCrossingCount, currentOsmDistance, 680),
            wayorder: currentWayOrder
          });




          // For Email Samples
          rawWacAnalysisSetList.add({
            osmid: currentOsmDetails.osmid,
            wac: wacComputeScore(curWaclslswls, curWacwdnl, curWacwtnlstc, curWacwsnlslc, curWacwwnlwcwrs, curWacwkikcck, curWacwccrccc),
            l: 5,
            kerbtype: currentOsmDetails.kerb,
            ikc: currentOsmDetails.wacInaccessKerb,
            slope: currentOsmDetails.incline,
            surfacetype: currentOsmDetails.surface,
            stc: currentOsmDetails.wacStc,
            slc: currentOsmDetails.wacSlopeCost,
            width: currentOsmDetails.width,
            wrs: currentOsmDetails.wacWidthRelevance,
            wc: currentOsmDetails.wacWidthCost,
            crossingtype: currentOsmDetails.crossing,
            crc: currentOsmDetails.wacCrossingCount,
            wsnlslc: normalizedSlopeCost(0.14, currentOsmDetails.wacSlopeCost, currentOsmDistance, 680),
            crossingtype: currentOsmDetails.crossing,
            kerbtype: currentOsmDetails.kerb,
            lslswls: wacLSlopeLimitWidthLimit(5, currentOsmDetails.sIncline, currentOsmDetails.sWidth),
            wdnl: normalizedLengthWeighted(0.17, currentOsmDistance, 680),
            wtnlstc: normalizedSurfaceTypeCost(0.19, currentOsmDetails.wacStc, currentOsmDistance, 680),
            wwnlwcwrs: normalizedWidthCostWidthRelevance(0.09, currentOsmDetails.wacWidthCost, currentOsmDetails.wacWidthRelevance, currentOsmDistance, 680),
            wkikcck: wacInaccessKerbScore(0.26, currentOsmDetails.wacInaccessKerb, 680),
            wccrccc: wacCrossingScore(0.15, currentOsmDetails.wacCrossingCount, currentOsmDistance, 680),
            distance: currentOsmDistance,
            segmentmaxlength: 680,
            normalizedlength: normalizedLength(currentOsmDistance, 680),
            startlongtitude: [...coordinatesSetList][nodeStartIndex].longtitude,
            startlatitude: [...coordinatesSetList][nodeStartIndex].latitude,
            endlongtitude: [...coordinatesSetList][nodeEndIndex].longtitude,
            endlatitude: [...coordinatesSetList][nodeEndIndex].latitude,
            wayorder: currentWayOrder
          });
        });

        // Total the Values of the WAC Data
        let finalWacScore = 0;
        wacDataSetList.forEach(el => {
          finalWacScore = finalWacScore + wacComputeScore(el.lslswls, el.wdnl, el.wtnlstc, el.wsnlslc, el.wwnlwcwrs, el.wkikcck, el.wccrccc);
        });
        let cleanFinalScore = parseFloat(finalWacScore.toFixed(6));
        let wacSuccess =
        {
          data: {
            analysisid: analysisid,
            iserror: false,
            payload: [...rawWacAnalysisSetList],
            wactotal: cleanFinalScore,
            totaldistance: simulation_rawdata.data.features[0].properties.summary.distance,
            totalduration: simulation_rawdata.data.features[0].properties.summary.duration,
            totalascent: simulation_rawdata.data.features[0].properties.ascent,
            totaldescent: simulation_rawdata.data.features[0].properties.descent,
            simulationtype: "uncorrected"
          }
        }

        const wacSaveResult = await axios.post(`${this.$config.ANALYSIS_CMS_ENDPOINT}/api/wac-uncorrecteds`, wacSuccess);
        if (wacSaveResult) { console.log(wacSaveResult); }
      } catch (err) {
        console.log("[PROCESS_SIMULATION_DATA_UNCORRECTED] [ANALYSIS] Error in Processing WAC Analysis : " + analysisid);
      }

    } catch (err) {
      console.log("[PROCESS_SIMULATION_DATA_UNCORRECTED] Error in Generating Simulation Route : " + analysisid);
      console.log(err);
      const calculationResults = await axios.post(`${this.$config.ANALYSIS_CMS_ENDPOINT}/api/wac-uncorrecteds`, {
        data: {
          analysisid: analysisid,
          payload: { error: err.message },
          iserror: true,
          wactotal: 0,
          totaldistance: 0,
          totalduration: 0,
          totalascent: 0,
          totaldescent: 0,
          simulationtype: "uncorrected"
        }
      });
      if (calculationResults) {
        console.log("[PROCESS_SIMULATION_DATA_UNCORRECTED] Successfully Processed Analysis ID : " + analysisid);
      }
    }
  },

  async PROCESS_SIMULATION_DATA_CORRECTED({ commit }, { analysisid = "", profileChoice = "wheelchair", origin, destination, maximum_incline = "20", minimum_width = 0.815, surface_type = "cobblestone", avoidPolygon = [] }) {

    let simulation_rawdata;
    // Default Raw Payloads

    let noAttrRawData;

    // Storage of Way IDS
    let osm_wayid_query = [];

    // Storage of Tags Attributes
    let storage_tags_attr = [];

    // Storage of Routing Coordinates
    let storage_routingCoord = [];

    // Storage of OSM Way Query;
    let osmWayQuery = "";

    //  Create a Segment Location Map    
    //  this set will contain the way index of the osmid
    const nodeSegmentMap = new Set();
    const elevationSetList = new Set();
    const coordinatesSetList = new Set();
    const rawWacSetList = new Set();
    const lineMarkerSetList = new Set();
    const osmDistanceSetList = new Set();
    const wacDataSetList = new Set();
    const rawWacAnalysisSetList = new Set();

    // Routing Options
    let myRoutingOption = {
      profile_params: {
        restrictions: {
          maximum_incline: maximum_incline,
          minimum_width: minimum_width,
          surface_type: surface_type
        },
        surface_quality_known: false,
        allow_unsuitable: false
      },
      avoid_polygons: {
        type: "MultiPolygon",
        coordinates: avoidPolygon
      }
    };

    // Routing Parameter Check
    if (avoidPolygon.length == 0) {
      delete myRoutingOption['avoid_polygons'];
    }
    if (maximum_incline == 0) {
      delete myRoutingOption.profile_params.restrictions.maximum_incline;
    }
    if (profileChoice != 'wheelchair') {
      delete myRoutingOption['profile_params'];
    }

    // Run Request
    try {
      simulation_rawdata = await axios.post(`${this.$config.CORRECTED_ORS_ENDPOINT}/v2/directions/${profileChoice}/geojson`, {
        coordinates: [[origin[1], origin[0]], [destination[1], destination[0]]],
        extra_info: ["osmid", "waytype", "steepness"],
        elevation: true,
        preference: "recommended",
        options: myRoutingOption
      }
      );
      if (simulation_rawdata) {
        // prettier-ignore
        for (let itr = 0; itr < simulation_rawdata.data.features[0].geometry.coordinates.length; itr++) {
          coordinatesSetList.add({ id: itr, latitude: simulation_rawdata.data.features[0].geometry.coordinates[itr][1], longtitude: simulation_rawdata.data.features[0].geometry.coordinates[itr][0] });
          storage_routingCoord.push([simulation_rawdata.data.features[0].geometry.coordinates[itr][1], simulation_rawdata.data.features[0].geometry.coordinates[itr][0]]);
          elevationSetList.add(simulation_rawdata.data.features[0].geometry.coordinates[itr][2]);
        }
        // prettier-ignore
        if (simulation_rawdata.data.features[0].properties.extras.osmId) {
          for (let i = 0; i < simulation_rawdata.data.features[0].properties.extras.osmId.values.length; i++) {
            osmDistanceSetList.add({ osmid: simulation_rawdata.data.features[0].properties.extras.osmId.summary[i].value, distance: simulation_rawdata.data.features[0].properties.extras.osmId.summary[i].distance, amount: simulation_rawdata.data.features[0].properties.extras.osmId.summary[i].amount, wayorder: simulation_rawdata.data.features[0].properties.extras.osmId.values[i][0] });
            // Place the Segment in the list to map array
            nodeSegmentMap.add({ osmid: simulation_rawdata.data.features[0].properties.extras.osmId.values[i][2], startIndex: simulation_rawdata.data.features[0].properties.extras.osmId.values[i][0], endIndex: simulation_rawdata.data.features[0].properties.extras.osmId.values[i][1] });
            let myArr = simulation_rawdata.data.features[0].properties.extras.osmId.values[i];
            osm_wayid_query.push(myArr.slice(-1).pop());
          }
        } else {
          console.log("[PROCESS_SIMULATION_DATA_CORRECTED] No OSM ID to be parsed.");
        }
        // Get OSM TAGS based on OSM ID from the Route Generated
        for (let iter in osm_wayid_query) {
          osmWayQuery = osmWayQuery + "way(" + osm_wayid_query[iter] + ");";
        }
        try {
          const myTagResult = await axios.get(`${this.$config.LOCAL_OVERPASS_ENDPOINT}/api/interpreter?data=[out:json];(${osmWayQuery});out%20tags;`);
          if (myTagResult) {
            Object.assign(storage_tags_attr, myTagResult.data.elements);
          }
        }
        catch (err) {
          console.log('[PROCESS_SIMULATION_DATA_CORRECTED] Error in Getting Tag Result');
          console.log(err);
        }
      }

      try {
        // Counters for the Statistics
        let tagCounter = _.size(storage_tags_attr);

        for (let itr = 0; itr < tagCounter; ++itr) {
          rawWacSetList.add({
            osmid: storage_tags_attr[itr].id,
            coordinates: storage_routingCoord[itr],
            surface: checkSurfaceType(storage_tags_attr[itr].tags.surface),
            highway: storage_tags_attr[itr].tags.highway,
            lit: checkLitValue(storage_tags_attr[itr].tags.lit),
            crossing: checkCrossingType(storage_tags_attr[itr].tags.crossing),
            incline: checkInclineValue(storage_tags_attr[itr].tags.incline),
            kerb: checkKerbType(storage_tags_attr[itr].tags.kerb),
            width: widthFormatter(storage_tags_attr[itr].tags.width),
          });
        }

        rawWacSetList.forEach(el => {
          // Search in the Segment osm  ID and assign lat long and assign the corresponding order
          let nodeStartIndex;
          let nodeEndIndex;
          let currentWayOrder;
          for (let e of nodeSegmentMap) {
            if (e.osmid === el.osmid) {
              nodeStartIndex = e.startIndex;
              nodeEndIndex = e.endIndex;
              currentWayOrder = e.startIndex;
              break;
            }
          }

          // Add the lat-lng value of the markers
          lineMarkerSetList.add([[...coordinatesSetList][nodeStartIndex].latitude, [...coordinatesSetList][nodeStartIndex].longtitude]);
          lineMarkerSetList.add([[...coordinatesSetList][nodeEndIndex].latitude, [...coordinatesSetList][nodeEndIndex].longtitude]);

          //  Search in Distance Set List the osmid and assign the corresponding distance of segment
          let currentOsmDistance;
          for (let d of osmDistanceSetList) {
            if (d.osmid === el.osmid) {
              currentOsmDistance = d.distance;
              break;
            }
          }


          // Set Data in the Object
          let currentOsmDetails = {};
          for (let x of rawWacSetList) {
            if (x.osmid === el.osmid) {
              currentOsmDetails.osmid = x.osmid;
              currentOsmDetails.surface = x.surface;
              currentOsmDetails.highway = x.highway;
              currentOsmDetails.lit = x.lit;
              currentOsmDetails.crossing = x.crossing;
              currentOsmDetails.incline = x.incline;
              currentOsmDetails.kerb = x.kerb;
              currentOsmDetails.width = x.width;
              currentOsmDetails.sWidth = wacWidthLimitSwitch(x.width);
              currentOsmDetails.sIncline = wacSlopeLimitSwitch(x.incline);
              currentOsmDetails.wacStc = wacSurfaceTypeCost(x.surface);
              currentOsmDetails.wacSlopeCost = wacSlopeCost(x.incline);
              currentOsmDetails.wacWidthCost = wacWidthCost(x.width);
              currentOsmDetails.wacWidthRelevance = wacWidthRelevanceSwitch(x.width);
              currentOsmDetails.wacInaccessKerb = wacInaccessibleKerbCount(x.kerb);
              currentOsmDetails.wacCrossingCount = wacCrossingCount(x.crossing);
            }
          }

          // Compute current WAC
          let curWaclslswls = wacLSlopeLimitWidthLimit(5, currentOsmDetails.sIncline, currentOsmDetails.sWidth);
          let curWacwdnl = normalizedLengthWeighted(0.17, currentOsmDistance, 680);
          let curWacwtnlstc = normalizedSurfaceTypeCost(0.19, currentOsmDetails.wacStc, currentOsmDistance, 680);
          let curWacwsnlslc = normalizedSlopeCost(0.14, currentOsmDetails.wacSlopeCost, currentOsmDistance, 680);
          let curWacwwnlwcwrs = normalizedWidthCostWidthRelevance(0.09, currentOsmDetails.wacWidthCost, currentOsmDetails.wacWidthRelevance, currentOsmDistance, 680);
          let curWacwkikcck = wacInaccessKerbScore(0.26, currentOsmDetails.wacInaccessKerb, 680);
          let curWacwccrccc = wacCrossingScore(0.15, currentOsmDetails.wacCrossingCount, currentOsmDistance, 680);


          // Parse and Transform Data
          wacDataSetList.add({
            osmid: currentOsmDetails.osmid,
            wac: wacComputeScore(curWaclslswls, curWacwdnl, curWacwtnlstc, curWacwsnlslc, curWacwwnlwcwrs, curWacwkikcck, curWacwccrccc),
            l: 5,
            sls: currentOsmDetails.sIncline,
            wls: currentOsmDetails.sWidth,
            nl: normalizedLength(currentOsmDistance, 680),
            wdnl: normalizedLengthWeighted(0.17, currentOsmDistance, 680),
            lslswls: wacLSlopeLimitWidthLimit(5, currentOsmDetails.sIncline, currentOsmDetails.sWidth),
            stc: currentOsmDetails.wacStc,
            wtnlstc: normalizedSurfaceTypeCost(0.19, currentOsmDetails.wacStc, currentOsmDistance, 680),
            slc: currentOsmDetails.wacSlopeCost,
            wsnlslc: normalizedSlopeCost(0.14, currentOsmDetails.wacSlopeCost, currentOsmDistance, 680),
            wc: currentOsmDetails.wacWidthCost,
            wrs: currentOsmDetails.wacWidthRelevance,
            wwnlwcwrs: normalizedWidthCostWidthRelevance(0.09, currentOsmDetails.wacWidthCost, currentOsmDetails.wacWidthRelevance, currentOsmDistance, 680),
            ikc: currentOsmDetails.wacInaccessKerb,
            ck: ckScore(680),
            wkikcck: wacInaccessKerbScore(0.26, currentOsmDetails.wacInaccessKerb, 680),
            crc: currentOsmDetails.wacCrossingCount,
            cc: ccScore(680),
            wccrccc: wacCrossingScore(0.15, currentOsmDetails.wacCrossingCount, currentOsmDistance, 680),
            wayorder: currentWayOrder
          });




          // For Email Samples
          rawWacAnalysisSetList.add({
            osmid: currentOsmDetails.osmid,
            wac: wacComputeScore(curWaclslswls, curWacwdnl, curWacwtnlstc, curWacwsnlslc, curWacwwnlwcwrs, curWacwkikcck, curWacwccrccc),
            l: 5,
            kerbtype: currentOsmDetails.kerb,
            ikc: currentOsmDetails.wacInaccessKerb,
            slope: currentOsmDetails.incline,
            surfacetype: currentOsmDetails.surface,
            stc: currentOsmDetails.wacStc,
            slc: currentOsmDetails.wacSlopeCost,
            width: currentOsmDetails.width,
            wrs: currentOsmDetails.wacWidthRelevance,
            wc: currentOsmDetails.wacWidthCost,
            crossingtype: currentOsmDetails.crossing,
            crc: currentOsmDetails.wacCrossingCount,
            wsnlslc: normalizedSlopeCost(0.14, currentOsmDetails.wacSlopeCost, currentOsmDistance, 680),
            crossingtype: currentOsmDetails.crossing,
            kerbtype: currentOsmDetails.kerb,
            lslswls: wacLSlopeLimitWidthLimit(5, currentOsmDetails.sIncline, currentOsmDetails.sWidth),
            wdnl: normalizedLengthWeighted(0.17, currentOsmDistance, 680),
            wtnlstc: normalizedSurfaceTypeCost(0.19, currentOsmDetails.wacStc, currentOsmDistance, 680),
            wwnlwcwrs: normalizedWidthCostWidthRelevance(0.09, currentOsmDetails.wacWidthCost, currentOsmDetails.wacWidthRelevance, currentOsmDistance, 680),
            wkikcck: wacInaccessKerbScore(0.26, currentOsmDetails.wacInaccessKerb, 680),
            wccrccc: wacCrossingScore(0.15, currentOsmDetails.wacCrossingCount, currentOsmDistance, 680),
            distance: currentOsmDistance,
            segmentmaxlength: 680,
            normalizedlength: normalizedLength(currentOsmDistance, 680),
            startlongtitude: [...coordinatesSetList][nodeStartIndex].longtitude,
            startlatitude: [...coordinatesSetList][nodeStartIndex].latitude,
            endlongtitude: [...coordinatesSetList][nodeEndIndex].longtitude,
            endlatitude: [...coordinatesSetList][nodeEndIndex].latitude,
            wayorder: currentWayOrder
          });
        });

        // Total the Values of the WAC Data
        let finalWacScore = 0;
        wacDataSetList.forEach(el => {
          finalWacScore = finalWacScore + wacComputeScore(el.lslswls, el.wdnl, el.wtnlstc, el.wsnlslc, el.wwnlwcwrs, el.wkikcck, el.wccrccc);
        });
        let cleanFinalScore = parseFloat(finalWacScore.toFixed(6));


        let wacSuccess =
        {
          data: {
            analysisid: analysisid,
            iserror: false,
            payload: [...rawWacAnalysisSetList],
            wactotal: cleanFinalScore,
            totaldistance: simulation_rawdata.data.features[0].properties.summary.distance,
            totalduration: simulation_rawdata.data.features[0].properties.summary.duration,
            totalascent: simulation_rawdata.data.features[0].properties.ascent,
            totaldescent: simulation_rawdata.data.features[0].properties.descent,
            simulationtype: "complete"
          }
        }

        const wacSaveResult = await axios.post(`${this.$config.ANALYSIS_CMS_ENDPOINT}/api/wac-correcteds`, wacSuccess);
        if (wacSaveResult) { console.log(wacSaveResult); }
      } catch (err) {
        console.log("[PROCESS_SIMULATION_DATA_CORRECTED] [ANALYSIS] Error in Processing WAC Analysis : " + analysisid);
      }

    } catch (err) {
      console.log("[PROCESS_SIMULATION_DATA_CORRECTED] Error in Generating Simulation Route : " + analysisid);
      console.log(err);
      const calculationResults = await axios.post(`${this.$config.ANALYSIS_CMS_ENDPOINT}/api/wac-correcteds`, {
        data: {
          analysisid: analysisid,
          payload: { error: err.message },
          iserror: true,
          wactotal: 0,
          totaldistance: 0,
          totalduration: 0,
          totalascent: 0,
          totaldescent: 0,
          simulationtype: "complete"
        }
      });
      if (calculationResults) {
        console.log("[PROCESS_SIMULATION_DATA_CORRECTED] Successfully Processed Analysis ID : " + analysisid);
      }
    }
  },
  async PROCESS_SIMULATION_DATA_NOATTRIBUTE({ commit }, { analysisid = "", profileChoice = "wheelchair", origin, destination, maximum_incline = "20", minimum_width = 0.815, surface_type = "cobblestone", avoidPolygon = [] }) {

    let simulation_rawdata;

    // Storage of Way IDS
    let osm_wayid_query = [];

    // Storage of Tags Attributes
    let storage_tags_attr = [];

    // Storage of Routing Coordinates
    let storage_routingCoord = [];

    // Storage of OSM Way Query;
    let osmWayQuery = "";

    //  Create a Segment Location Map    
    //  this set will contain the way index of the osmid
    const nodeSegmentMap = new Set();
    const elevationSetList = new Set();
    const coordinatesSetList = new Set();
    const rawWacSetList = new Set();
    const lineMarkerSetList = new Set();
    const osmDistanceSetList = new Set();
    const wacDataSetList = new Set();
    const rawWacAnalysisSetList = new Set();

    // Routing Options
    let myRoutingOption = {
      profile_params: {
        restrictions: {
          maximum_incline: maximum_incline,
          minimum_width: minimum_width,
          surface_type: surface_type
        },
        surface_quality_known: false,
        allow_unsuitable: false
      },
      avoid_polygons: {
        type: "MultiPolygon",
        coordinates: avoidPolygon
      }
    };

    // Routing Parameter Check
    if (avoidPolygon.length == 0) {
      delete myRoutingOption['avoid_polygons'];
    }
    if (maximum_incline == 0) {
      delete myRoutingOption.profile_params.restrictions.maximum_incline;
    }
    if (profileChoice != 'wheelchair') {
      delete myRoutingOption['profile_params'];
    }

    // Run Request
    try {
      simulation_rawdata = await axios.post(`${this.$config.NOATTRIBUTE_ORS_ENDPOINT}/v2/directions/${profileChoice}/geojson`, {
        coordinates: [[origin[1], origin[0]], [destination[1], destination[0]]],
        extra_info: ["osmid", "waytype", "steepness"],
        elevation: true,
        preference: "recommended",
        options: myRoutingOption
      }
      );
      if (simulation_rawdata) {
        // prettier-ignore
        for (let itr = 0; itr < simulation_rawdata.data.features[0].geometry.coordinates.length; itr++) {
          coordinatesSetList.add({ id: itr, latitude: simulation_rawdata.data.features[0].geometry.coordinates[itr][1], longtitude: simulation_rawdata.data.features[0].geometry.coordinates[itr][0] });
          storage_routingCoord.push([simulation_rawdata.data.features[0].geometry.coordinates[itr][1], simulation_rawdata.data.features[0].geometry.coordinates[itr][0]]);
          elevationSetList.add(simulation_rawdata.data.features[0].geometry.coordinates[itr][2]);
        }
        // prettier-ignore
        if (simulation_rawdata.data.features[0].properties.extras.osmId) {
          for (let i = 0; i < simulation_rawdata.data.features[0].properties.extras.osmId.values.length; i++) {
            osmDistanceSetList.add({ osmid: simulation_rawdata.data.features[0].properties.extras.osmId.summary[i].value, distance: simulation_rawdata.data.features[0].properties.extras.osmId.summary[i].distance, amount: simulation_rawdata.data.features[0].properties.extras.osmId.summary[i].amount, wayorder: simulation_rawdata.data.features[0].properties.extras.osmId.values[i][0] });
            // Place the Segment in the list to map array
            nodeSegmentMap.add({ osmid: simulation_rawdata.data.features[0].properties.extras.osmId.values[i][2], startIndex: simulation_rawdata.data.features[0].properties.extras.osmId.values[i][0], endIndex: simulation_rawdata.data.features[0].properties.extras.osmId.values[i][1] });
            let myArr = simulation_rawdata.data.features[0].properties.extras.osmId.values[i];
            osm_wayid_query.push(myArr.slice(-1).pop());
          }
        } else {
          console.log("No OSM ID to be parsed.");
        }
        // Get OSM TAGS based on OSM ID from the Route Generated
        for (let iter in osm_wayid_query) {
          osmWayQuery = osmWayQuery + "way(" + osm_wayid_query[iter] + ");";
        }
        try {
          const myTagResult = await axios.get(`${this.$config.LOCAL_OVERPASS_ENDPOINT}/api/interpreter?data=[out:json];(${osmWayQuery});out%20tags;`);
          if (myTagResult) {
            Object.assign(storage_tags_attr, myTagResult.data.elements);
          }
        }
        catch (err) {
          console.log('[PROCESS_SIMULATION_DATA_NOATTRIBUTE] Error in Getting Tag Result');
          console.log(err);
        }
      }

      try {
        // Counters for the Statistics
        let tagCounter = _.size(storage_tags_attr);

        for (let itr = 0; itr < tagCounter; ++itr) {
          rawWacSetList.add({
            osmid: storage_tags_attr[itr].id,
            coordinates: storage_routingCoord[itr],
            surface: checkSurfaceType(storage_tags_attr[itr].tags.surface),
            highway: storage_tags_attr[itr].tags.highway,
            lit: checkLitValue(storage_tags_attr[itr].tags.lit),
            crossing: checkCrossingType(storage_tags_attr[itr].tags.crossing),
            incline: checkInclineValue(storage_tags_attr[itr].tags.incline),
            kerb: checkKerbType(storage_tags_attr[itr].tags.kerb),
            width: widthFormatter(storage_tags_attr[itr].tags.width),
          });
        }

        rawWacSetList.forEach(el => {
          // Search in the Segment osm  ID and assign lat long and assign the corresponding order
          let nodeStartIndex;
          let nodeEndIndex;
          let currentWayOrder;
          for (let e of nodeSegmentMap) {
            if (e.osmid === el.osmid) {
              nodeStartIndex = e.startIndex;
              nodeEndIndex = e.endIndex;
              currentWayOrder = e.startIndex;
              break;
            }
          }

          // Add the lat-lng value of the markers
          lineMarkerSetList.add([[...coordinatesSetList][nodeStartIndex].latitude, [...coordinatesSetList][nodeStartIndex].longtitude]);
          lineMarkerSetList.add([[...coordinatesSetList][nodeEndIndex].latitude, [...coordinatesSetList][nodeEndIndex].longtitude]);

          //  Search in Distance Set List the osmid and assign the corresponding distance of segment
          let currentOsmDistance;
          for (let d of osmDistanceSetList) {
            if (d.osmid === el.osmid) {
              currentOsmDistance = d.distance;
              break;
            }
          }


          // Set Data in the Object
          let currentOsmDetails = {};
          for (let x of rawWacSetList) {
            if (x.osmid === el.osmid) {
              currentOsmDetails.osmid = x.osmid;
              currentOsmDetails.surface = x.surface;
              currentOsmDetails.highway = x.highway;
              currentOsmDetails.lit = x.lit;
              currentOsmDetails.crossing = x.crossing;
              currentOsmDetails.incline = x.incline;
              currentOsmDetails.kerb = x.kerb;
              currentOsmDetails.width = x.width;
              currentOsmDetails.sWidth = wacWidthLimitSwitch(x.width);
              currentOsmDetails.sIncline = wacSlopeLimitSwitch(x.incline);
              currentOsmDetails.wacStc = wacSurfaceTypeCost(x.surface);
              currentOsmDetails.wacSlopeCost = wacSlopeCost(x.incline);
              currentOsmDetails.wacWidthCost = wacWidthCost(x.width);
              currentOsmDetails.wacWidthRelevance = wacWidthRelevanceSwitch(x.width);
              currentOsmDetails.wacInaccessKerb = wacInaccessibleKerbCount(x.kerb);
              currentOsmDetails.wacCrossingCount = wacCrossingCount(x.crossing);
            }
          }

          // Compute current WAC
          let curWaclslswls = wacLSlopeLimitWidthLimit(5, currentOsmDetails.sIncline, currentOsmDetails.sWidth);
          let curWacwdnl = normalizedLengthWeighted(0.17, currentOsmDistance, 680);
          let curWacwtnlstc = normalizedSurfaceTypeCost(0.19, currentOsmDetails.wacStc, currentOsmDistance, 680);
          let curWacwsnlslc = normalizedSlopeCost(0.14, currentOsmDetails.wacSlopeCost, currentOsmDistance, 680);
          let curWacwwnlwcwrs = normalizedWidthCostWidthRelevance(0.09, currentOsmDetails.wacWidthCost, currentOsmDetails.wacWidthRelevance, currentOsmDistance, 680);
          let curWacwkikcck = wacInaccessKerbScore(0.26, currentOsmDetails.wacInaccessKerb, 680);
          let curWacwccrccc = wacCrossingScore(0.15, currentOsmDetails.wacCrossingCount, currentOsmDistance, 680);


          // Parse and Transform Data
          wacDataSetList.add({
            osmid: currentOsmDetails.osmid,
            wac: wacComputeScore(curWaclslswls, curWacwdnl, curWacwtnlstc, curWacwsnlslc, curWacwwnlwcwrs, curWacwkikcck, curWacwccrccc),
            l: 5,
            sls: currentOsmDetails.sIncline,
            wls: currentOsmDetails.sWidth,
            nl: normalizedLength(currentOsmDistance, 680),
            wdnl: normalizedLengthWeighted(0.17, currentOsmDistance, 680),
            lslswls: wacLSlopeLimitWidthLimit(5, currentOsmDetails.sIncline, currentOsmDetails.sWidth),
            stc: currentOsmDetails.wacStc,
            wtnlstc: normalizedSurfaceTypeCost(0.19, currentOsmDetails.wacStc, currentOsmDistance, 680),
            slc: currentOsmDetails.wacSlopeCost,
            wsnlslc: normalizedSlopeCost(0.14, currentOsmDetails.wacSlopeCost, currentOsmDistance, 680),
            wc: currentOsmDetails.wacWidthCost,
            wrs: currentOsmDetails.wacWidthRelevance,
            wwnlwcwrs: normalizedWidthCostWidthRelevance(0.09, currentOsmDetails.wacWidthCost, currentOsmDetails.wacWidthRelevance, currentOsmDistance, 680),
            ikc: currentOsmDetails.wacInaccessKerb,
            ck: ckScore(680),
            wkikcck: wacInaccessKerbScore(0.26, currentOsmDetails.wacInaccessKerb, 680),
            crc: currentOsmDetails.wacCrossingCount,
            cc: ccScore(680),
            wccrccc: wacCrossingScore(0.15, currentOsmDetails.wacCrossingCount, currentOsmDistance, 680),
            wayorder: currentWayOrder
          });




          // For Email Samples
          rawWacAnalysisSetList.add({
            osmid: currentOsmDetails.osmid,
            wac: wacComputeScore(curWaclslswls, curWacwdnl, curWacwtnlstc, curWacwsnlslc, curWacwwnlwcwrs, curWacwkikcck, curWacwccrccc),
            l: 5,
            kerbtype: currentOsmDetails.kerb,
            ikc: currentOsmDetails.wacInaccessKerb,
            slope: currentOsmDetails.incline,
            surfacetype: currentOsmDetails.surface,
            stc: currentOsmDetails.wacStc,
            slc: currentOsmDetails.wacSlopeCost,
            width: currentOsmDetails.width,
            wrs: currentOsmDetails.wacWidthRelevance,
            wc: currentOsmDetails.wacWidthCost,
            crossingtype: currentOsmDetails.crossing,
            crc: currentOsmDetails.wacCrossingCount,
            wsnlslc: normalizedSlopeCost(0.14, currentOsmDetails.wacSlopeCost, currentOsmDistance, 680),
            crossingtype: currentOsmDetails.crossing,
            kerbtype: currentOsmDetails.kerb,
            lslswls: wacLSlopeLimitWidthLimit(5, currentOsmDetails.sIncline, currentOsmDetails.sWidth),
            wdnl: normalizedLengthWeighted(0.17, currentOsmDistance, 680),
            wtnlstc: normalizedSurfaceTypeCost(0.19, currentOsmDetails.wacStc, currentOsmDistance, 680),
            wwnlwcwrs: normalizedWidthCostWidthRelevance(0.09, currentOsmDetails.wacWidthCost, currentOsmDetails.wacWidthRelevance, currentOsmDistance, 680),
            wkikcck: wacInaccessKerbScore(0.26, currentOsmDetails.wacInaccessKerb, 680),
            wccrccc: wacCrossingScore(0.15, currentOsmDetails.wacCrossingCount, currentOsmDistance, 680),
            distance: currentOsmDistance,
            segmentmaxlength: 680,
            normalizedlength: normalizedLength(currentOsmDistance, 680),
            startlongtitude: [...coordinatesSetList][nodeStartIndex].longtitude,
            startlatitude: [...coordinatesSetList][nodeStartIndex].latitude,
            endlongtitude: [...coordinatesSetList][nodeEndIndex].longtitude,
            endlatitude: [...coordinatesSetList][nodeEndIndex].latitude,
            wayorder: currentWayOrder
          });
        });

        // Total the Values of the WAC Data
        let finalWacScore = 0;
        wacDataSetList.forEach(el => {
          finalWacScore = finalWacScore + wacComputeScore(el.lslswls, el.wdnl, el.wtnlstc, el.wsnlslc, el.wwnlwcwrs, el.wkikcck, el.wccrccc);
        });
        let cleanFinalScore = parseFloat(finalWacScore.toFixed(6));


        let wacSuccess =
        {
          data: {
            analysisid: analysisid,
            iserror: false,
            payload: [...rawWacAnalysisSetList],
            wactotal: cleanFinalScore,
            totaldistance: simulation_rawdata.data.features[0].properties.summary.distance,
            totalduration: simulation_rawdata.data.features[0].properties.summary.duration,
            totalascent: simulation_rawdata.data.features[0].properties.ascent,
            totaldescent: simulation_rawdata.data.features[0].properties.descent,
            simulationtype: "noattribute"
          }
        }

        const wacSaveResult = await axios.post(`${this.$config.ANALYSIS_CMS_ENDPOINT}/api/wac-noattributes`, wacSuccess);
        if (wacSaveResult) { console.log(wacSaveResult); }
      } catch (err) {
        console.log("[PROCESS_SIMULATION_DATA_NOATTRIBUTE] [ANALYSIS] Error in Processing WAC Analysis : " + analysisid);
      }

    } catch (err) {
      console.log("[PROCESS_SIMULATION_DATA_NOATTRIBUTE] Error in Generating Simulation Route : " + analysisid);
      console.log(err);
      const calculationResults = await axios.post(`${this.$config.ANALYSIS_CMS_ENDPOINT}/api/wac-noattributes`, {
        data: {
          analysisid: analysisid,
          payload: { error: err.message },
          iserror: true,
          wactotal: 0,
          totaldistance: 0,
          totalduration: 0,
          totalascent: 0,
          totaldescent: 0,
          simulationtype: "noattribute"
        }
      });
      if (calculationResults) {
        console.log("[PROCESS_SIMULATION_DATA_NOATTRIBUTE] Successfully Processed Analysis ID : " + analysisid);
      }
    }
  },
  // This Function is used for the Statistical Page Simulation Bulk
  // async CORRECTED_PROCESS_SIMULATION_DATA({ commit }, { origin, destination, maximum_incline = "20", surface_type = "cobblestone", profileChoice = "wheelchair", inclinepref = 15, surface_pref = 6, analysisid = "" }) {
  //   let curAnalysisId = analysisid;
  //   try {
  //   } catch (err) {
  //     let raiDataPayload = {
  //       data: {
  //         analysisid: curAnalysisId,
  //         iserror: true,
  //       }

  //     };
  //     let raiSave = await axios.post(`${this.$config.ANALYSIS_CMS_ENDPOINT}/api/corindices`, raiDataPayload);
  //     if (raiSave) {
  //       console.log("Successfully Processed Analysis ID : " + curAnalysisId);
  //     }
  //     console.log("[CORRECTED_PROCESS_SIMULATION_DATA] Error in Generating Recommended Route");
  //     console.log(err);
  //   }
  // },

  // This Function is used for Statistical Page Simulation Bulk
  // async NOATTR_PROCESS_SIMULATION_DATA({ commit }, { origin, destination, maximum_incline = "20", surface_type = "cobblestone", profileChoice = "wheelchair", inclinepref = 15, surface_pref = 6, analysisid = "" }) {
  //   let curAnalysisId = analysisid;
  //   try {
  //   } catch (err) {
  //     let raiDataPayload = {
  //       data: {
  //         analysisid: curAnalysisId,
  //         iserror: true,
  //       }

  //     };
  //     let raiSave = await axios.post(`${this.$config.ANALYSIS_CMS_ENDPOINT}/api/corindices`, raiDataPayload);
  //     if (raiSave) {
  //       console.log("Successfully Processed Analysis ID : " + curAnalysisId);
  //     }
  //     console.log("[NOATTR_PROCESS_SIMULATION_DATA] Error in Generating Recommended Route");
  //     console.log(err);
  //   }
  // },

  async CLEAR_ROUTING_VALUES({ commit }) {
    try {
      commit("DELETE_ROUTING_VALUES");
      commit("CLEAR_CHOSEN_COORDINATES");
    } catch (err) {
      console.log(err);
    }
  },
  async CLEAR_ROUTING_MARKERS({ commit }) {
    try {
      commit("DELETE_COORDINATE_MARKER_LOCATION");
    } catch (err) {
      console.log(err);
    }
  },
  async GET_USER_GPS_LOCATION({ commit }) {
    try {

    } catch (err) {
      console.log(err);
    }
  }
};
