export default {
  GET_MARKER_LIST(state) {
    return state.markerList;
  },

  GET_START_ENDPOINT(state) {

    let lat;
    let lng;
    let myVal;

    if (state.startMarkerLocation.lat === undefined) {
      lat = "";
    } else {
      lat = state.startMarkerLocation.lat;
    }
    if (state.startMarkerLocation.lng === undefined) {
      lng = "";
    } else {
      lng = state.startMarkerLocation.lng;
    }

    if (state.startMarkerLocation.lat === undefined) {
      myVal = ``;
    } else {
      myVal = `${lat} , ${lng}`;
    }
    return myVal;
  },
  GET_END_ENDPOINT(state) {
    let lat;
    let lng;
    let myVal;
    if (state.endMarkerLocation.lat === undefined) {
      lat = "";
    } else {
      lat = state.endMarkerLocation.lat;
    }
    if (state.endMarkerLocation.lng === undefined) {
      lng = "";
    } else {
      lng = state.endMarkerLocation.lng;
    }
    if (state.endMarkerLocation.lat === undefined) {
      myVal = ``;
    } else {
      myVal = `${lat} , ${lng}`;
    }
    return myVal;
  },
  GET_START_ENDPOINT_TOGGLE(state) {
    return state.startMarkerToggle;
  },
  GET_END_ENDPOINT_TOGGLE(state) {
    return state.endMarkerToggle;
  },
  GET_LOCAL_WAYID_LIST(state) {
    return state.osmWayNodeList;
  },
  GET_LOCAL_MAP_DIRECTIONS(state) {
    return state.localDirectionSteps;
  },
  GET_LIVE_MAP_DIRECTIONS(state) {
    return state.liveDirectionSteps;
  },
  GET_TAG_FEATURE_LIST(state) {
    return state.tagFeatureList;
  },
  GET_LOCAL_ROUTE_DURATION(state) {
    return state.rawLocalRouteData;
  },
  PROCESSED_LOCAL_POLYLINE(state) {
    return state.localPolylineCoordinates;
  },
  PROCESSED_LIVE_POLYLINE(state) {
    return state.livePolylineCoordinates;
  },

  // Barrier Getters
  GET_BARRIER_COORDINATES(state) {
    return state.barrierCoordinates;
  },
  GET_BARRIER_TOGGLE_STATUS(state) {
    return state.barrierToggle;
  },
  GET_BUFFERED_BARRIERS(state) {
    return state.bufferedBarrierCoordinates;
  },
  // GET_PAYLOAD_BUFFERED_BARRIERS(state){
  //   return state.bufferedPayloadBarrierCoordinates;
  // },

  // Local Shortest Route Getters
  SRT_LCL_TAGLIST(state) {
    return state.srt_lcl_tagFeatureList;
  },
  SRT_LCL_ROUTE_COORDINATES(state) {
    return state.srt_lcl_polylineCoordinates;
  },
  SRT_LCL_ROUTE_ASCENT(state) {
    return state.srt_lcl_rawLocalRouteData.features[0].properties.ascent;
  },
  SRT_LCL_ROUTE_DESCENT(state) {
    return state.srt_lcl_rawLocalRouteData.features[0].properties.descent;
  },
  SRT_LCL_ROUTE_DURATION(state) {
    return (state.srt_lcl_rawLocalRouteData.features[0].properties.summary.duration / 60).toFixed(1);
  },
  SRT_LCL_ROUTE_DISTANCE(state) {
    return state.srt_lcl_rawLocalRouteData.features[0].properties.summary.distance;
  },
  SRT_LCL_ROUTE_ELEVATION(state) {
    let formatElevation = {
      series: [
        {
          name: "Elevation",
          data: state.srt_lcl_elevation
        },
      ]

    };
    return formatElevation;
  },

  // Local Fastest Route Getters
  FTS_LCL_TAGLIST(state) {
    return state.fts_lcl_tagFeatureList;
  },
  FTS_LCL_ROUTE_COORDINATES(state) {
    return state.fts_lcl_polylineCoordinates;
  },
  FTS_LCL_ROUTE_DURATION(state) {
    return (state.fts_lcl_rawLocalRouteData.features[0].properties.summary.duration / 60).toFixed(1);
  },
  FTS_LCL_ROUTE_DISTANCE(state) {
    return state.fts_lcl_rawLocalRouteData.features[0].properties.summary.distance;
  },
  FTS_LCL_ROUTE_ASCENT(state) {
    return state.fts_lcl_rawLocalRouteData.features[0].properties.ascent;
  },
  FTS_LCL_ROUTE_DESCENT(state) {
    return state.fts_lcl_rawLocalRouteData.features[0].properties.descent;
  },

  // Local Recommended Route Getters
  REC_LCL_ROUTE_COORDINATES(state) {
    return state.rec_lcl_polylineCoordinates;
  },
  REC_LCL_TAGLIST(state) {
    return state.rec_lcl_tagFeatureList;
  },
  REC_LCL_ROUTE_DURATION(state) {
    return (state.rec_lcl_rawLocalRouteData.features[0].properties.summary.duration / 60).toFixed(1);
  },
  REC_LCL_ROUTE_DISTANCE(state) {
    return state.rec_lcl_rawLocalRouteData.features[0].properties.summary.distance;
  },
  REC_LCL_ROUTE_ASCENT(state) {
    return state.rec_lcl_rawLocalRouteData.features[0].properties.ascent;
  },
  REC_LCL_ROUTE_DESCENT(state) {
    return state.rec_lcl_rawLocalRouteData.features[0].properties.descent;
  },
  REC_LCL_ROUTE_ELEVATION(state) {
    let formatElevation = {
      series: [
        {
          name: "Elevation",
          data: state.rec_lcl_elevation
        },
      ]

    };
    return formatElevation;
  },

  // Live Shortest Route Getters
  SRT_LIV_ROUTE_COORDINATES(state) {
    return state.srt_liv_polylineCoordinates;
  },
  SRT_LIV_TAGLIST(state) {
    return state.srt_liv_tagFeatureList;
  },
  SRT_LIV_ROUTE_DURATION(state) {
    return (state.srt_liv_rawLocalRouteData.features[0].properties.summary.duration / 60).toFixed(1)
  },
  SRT_LIV_ROUTE_DISTANCE(state) {
    return state.srt_liv_rawLocalRouteData.features[0].properties.summary.distance
  },
  SRT_LIV_ROUTE_ASCENT(state) {
    return state.srt_liv_rawLocalRouteData.features[0].properties.ascent
  },
  SRT_LIV_ROUTE_DESCENT(state) {
    return state.srt_liv_rawLocalRouteData.features[0].properties.descent
  },

  // Live Fastest Route Getters
  FTS_LIV_ROUTE_COORDINATES(state) {
    return state.fts_liv_polylineCoordinates;
  },
  FTS_LIV_TAGLIST(state) {
    return state.fts_liv_tagFeatureList;
  },
  FTS_LIV_ROUTE_DURATION(state) {
    return (state.fts_liv_rawLocalRouteData.features[0].properties.summary.duration / 60).toFixed(1)
  },
  FTS_LIV_ROUTE_DISTANCE(state) {
    return state.fts_liv_rawLocalRouteData.features[0].properties.summary.distance
  },
  FTS_LIV_ROUTE_ASCENT(state) {
    return state.fts_liv_rawLocalRouteData.features[0].properties.ascent
  },
  FTS_LIV_ROUTE_DESCENT(state) {
    return state.fts_liv_rawLocalRouteData.features[0].properties.descent
  },

  // Live Recommended Route Getters
  REC_LIV_ROUTE_COORDINATES(state) {
    return state.rec_liv_polylineCoordinates;
  },
  REC_LIV_TAGLIST(state) {
    return state.rec_liv_tagFeatureList;
  },
  REC_LIV_ROUTE_DURATION(state) {
    return (state.rec_liv_rawLocalRouteData.features[0].properties.summary.duration / 60).toFixed(1)
  },
  REC_LIV_ROUTE_DISTANCE(state) {
    return state.rec_liv_rawLocalRouteData.features[0].properties.summary.distance
  },
  REC_LIV_ROUTE_ASCENT(state) {
    return state.rec_liv_rawLocalRouteData.features[0].properties.ascent
  },
  REC_LIV_ROUTE_DESCENT(state) {
    return state.rec_liv_rawLocalRouteData.features[0].properties.descent
  },
  REC_LIV_ROUTE_ELEVATION(state) {
    let formatElevation = {
      series: [
        {
          name: "Elevation",
          data: state.rec_liv_elevation
        },
      ]

    };
    return formatElevation;
  },
  REC_LIV_ROUTE_INSTRUCTION(state) {
    return state.rec_liv_directionInstruction
  },


  // No Attribute
  GET_NOATTR_ROUTE_COORDINATES(state) {
    return state.noattr_polylineCoordinates;
  },
  GET_NOATTR_TAGLIST(state) {
    return state.noattr_tagFeatureList;
  },
  GET_NOATTR_ROUTE_DURATION(state) {
    return (state.noattr_rawLocalRouteData.features[0].properties.summary.duration / 60).toFixed(1)
  },
  GET_NOATTR_ROUTE_DISTANCE(state) {
    return state.noattr_rawLocalRouteData.features[0].properties.summary.distance
  },
  GET_NOATTR_ROUTE_ASCENT(state) {
    return state.noattr_rawLocalRouteData.features[0].properties.ascent
  },
  GET_NOATTR_ROUTE_DESCENT(state) {
    return state.noattr_rawLocalRouteData.features[0].properties.descent
  },
  GET_NOATTR_ROUTE_ELEVATION(state) {
    let formatElevation = {
      series: [
        {
          name: "Elevation",
          data: state.noattr_elevation
        },
      ]

    };
    return formatElevation;
  },
  GET_NOATTR_ROUTE_INSTRUCTION(state) {
    return state.noattr_directionInstruction
  },







  // Google Direction Getters
  GET_GCP_RAW_PAYLOAD(state) {
    return state.gcp_rawdata;
  },
  GET_GCP_ROUTE_COORDINATES(state) {
    return state.gcp_polylineCoordinates;
  },



  GET_LCL_RUNNING_ANALYSIS(state) {
    return state.rec_lcl_analysis;
  },
  GET_LIV_RUNNING_ANALYSIS(state) {
    return state.rec_liv_analysis;
  },
  GET_NOATTR_RUNNING_ANALYSIS(state){
    return state.noattr_analysis;
  },
  GET_WAC_UNCORRECTED_ANALYSIS(state) {
    return state.wacUncorrectedData;
  },
  GET_WAC_CORRECTED_ANALYSIS(state) {
    return state.wacCorrectedData;
  },
  GET_WAC_NOATTR_ANALYSIS(state) {
    return state.wacNoAttrData;
  },

  GET_WAC_CORRECTED_SCORE(state) {
    return state.wacCorrectedFinalScore;
  },
  GET_WAC_UNCORRECTED_SCORE(state) {
    return state.wacUncorrectedFinalScore;
  },
  GET_WAC_NOATTR_SCORE(state) {
    return state.wacNoAttrFinalScore;
  },

  GET_RAW_CORRECTED_WAC_DATA(state) {
    return state.rawCorrectedWacData
  },
  GET_RAW_UNCORRECTED_WAC_DATA(state) {
    return state.rawUncorrectedWacData;
  },
  GET_RAW_NOATTR_WAC_DATA(state) {
    return state.rawNoAttrWacData;
  },

  GET_COR_ROUTING_NODE_BUCKET(state) {
    return state.osmCorrectedNodeBucket;
  },
  GET_UNCOR_ROUTING_NODE_BUCKET(state) {
    return state.osmUncorrectedNodeBucket;
  },
  GET_NOATTR_ROUTING_NODE_BUCKET(state) {
    return state.osmNoattrNodeBucket;
  }



};
