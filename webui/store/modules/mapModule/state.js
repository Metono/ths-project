export default () => ({
  // Global Map Module Configuration
  startMarkerToggle: false,
  endMarkerToggle: false,
  tagFeatureList: [],
  poiFeatureList: [],
  startMarkerLocation: {},
  endMarkerLocation: {},
  markerList: [],


  // Barrier Rendering
  barrierToggle: false,
  barrierCoordinates: [],
  bufferedBarrierCoordinates: [],
  bufferedPayloadBarrierCoordinates: [],
  // LOCAL OSM SERVER DATA VALUES
  // Shortest
  srt_lcl_rawLocalRouteData: {},
  srt_lcl_osmWayNodeList: [],
  srt_lcl_tagFeatureList: [],
  srt_lcl_polylineCoordinates: [],
  srt_lcl_directionInstruction: [],
  srt_lcl_elevation: [],
  // Fastest
  fts_lcl_rawLocalRouteData: {},
  fts_lcl_osmWayNodeList: [],
  fts_lcl_tagFeatureList: [],
  fts_lcl_polylineCoordinates: [],
  fts_lcl_directionInstruction: [],
  fts_lcl_elevation: [],
  // Recommended
  rec_lcl_rawLocalRouteData: {},
  rec_lcl_osmWayNodeList: [],
  rec_lcl_tagFeatureList: [],
  rec_lcl_polylineCoordinates: [],
  rec_lcl_directionInstruction: [],
  rec_lcl_elevation: [],

  // LIVE OSM SERVER DATA VALUES

  // Shortest
  srt_liv_rawLocalRouteData: {},
  srt_liv_osmWayNodeList: [],
  srt_liv_tagFeatureList: [],
  srt_liv_polylineCoordinates: [],
  srt_liv_directionInstruction: [],
  srt_liv_elevation: [],
  // Fastest
  fts_liv_rawLocalRouteData: {},
  fts_liv_osmWayNodeList: [],
  fts_liv_tagFeatureList: [],
  fts_liv_polylineCoordinates: [],
  fts_liv_directionInstruction: [],
  fts_liv_elevation: [],
  // Recommended
  rec_liv_rawLocalRouteData: {},
  rec_liv_osmWayNodeList: [],
  rec_liv_tagFeatureList: [],
  rec_liv_polylineCoordinates: [],
  rec_liv_directionInstruction: [],
  rec_liv_elevation: [],


  // No Attribute
  noattr_rawLocalRouteData: {},
  noattr_osmWayNodeList: [],
  noattr_tagFeatureList: [],
  noattr_polylineCoordinates: [],
  noattr_directionInstruction: [],
  noattr_elevation: [],

  rawLocalRouteData: {},
  osmWayNodeList: [],
  localPolylineCoordinates: [],
  localDirectionSteps: [],
  wheelchairProfileRoute: {},
  elevationDetails: [],

  // GCP DIRECTION DATA
  gcp_rawdata: {},
  gcp_polylineCoordinates: [],
  gcp_directionInstruction: [],

  // Analysis Data
  rec_lcl_analysis: {},
  rec_liv_analysis: {},
  noattr_analysis: {},

  // LIVE OSM SERVER
  rawLiveRouteData: {},
  livePolylineCoordinates: [],
  liveDirectionSteps: [],

  // Wheelchair Cost Analysis Data
  rawUncorrectedWacData: {},
  wacUncorrectedData: {},
  wacUncorrectedFinalScore: 0,
  rawCorrectedWacData: {},
  wacCorrectedData: {},
  wacCorrectedFinalScore: 0,
  rawNoAttrWacData: {},
  wacNoAttrData: {},
  wacNoAttrFinalScore: 0,

  // Linestring Routing Points
  // This is to visualize the start and end of each segment based on points provided by the route
  osmCorrectedNodeBucket: [],
  osmUncorrectedNodeBucket: [],
  osmNoattrNodeBucket: [],
});

