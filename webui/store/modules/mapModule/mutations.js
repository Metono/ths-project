export default {
    // Global Settings
    TOGGLE_START_POINT_OPTION(state, payload) {
        state.startMarkerToggle = payload.curState;
    },
    TOGGLE_END_POINT_OPTION(state, payload) {
        state.endMarkerToggle = payload.curState;
    },
    SET_START_MARKER_LOCATION(state, payload) {
        state.startMarkerLocation = payload;
    },
    SET_END_MARKER_LOCATION(state, payload) {
        state.endMarkerLocation = payload
    },
    SET_WAYID_NODE_IDS(state, payload) {
        state.osmWayNodeList = payload;
    },
    SET_NODE_FEATURE_LIST(state, payload) {
        state.tagFeatureList = payload;
    },
    SET_LOCAL_ROUTING_DIRECTIONS(state, payload) {
        state.localDirectionSteps = payload;
    },
    SET_LOCAL_RAW_ROUTING_DATA(state, payload) {
        state.rawLocalRouteData = payload;
    },

    // Clearing Data Markers And Location Longtitude and Latitude
    CLEAR_CHOSEN_COORDINATES(state) {
        state.startMarkerLocation = {};
        state.endMarkerLocation = {};
    },
    CLEAR_WAYID_NODE_LIST(state) {
        state.osmWayNodeList = [];
    },
    CLEAR_ADDED_BARRIERS(state) {
        state.barrierCoordinates = [];
        state.bufferedBarrierCoordinates = [];
    },

    DELETE_COORDINATE_MARKER_LOCATION(state) {
        state.markerList = [];
    },
    DELETE_ROUTING_VALUES(state) {
        state.srt_lcl_rawLocalRouteData = {};
        state.srt_lcl_osmWayNodeList = [];
        state.srt_lcl_tagFeatureList = [];
        state.srt_lcl_directionInstruction = [];
        state.srt_lcl_elevation = [];
        state.srt_lcl_polylineCoordinates = [];

        state.fts_lcl_rawLocalRouteData = {};
        state.fts_lcl_osmWayNodeList = [];
        state.fts_lcl_tagFeatureList = [];
        state.fts_lcl_directionInstruction = [];
        state.fts_lcl_elevation = [];
        state.fts_lcl_polylineCoordinates = [];

        state.rec_lcl_rawLocalRouteData = {};
        state.rec_lcl_osmWayNodeList = [];
        state.rec_lcl_tagFeatureList = [];
        state.rec_lcl_directionInstruction = [];
        state.rec_lcl_elevation = [];
        state.rec_lcl_polylineCoordinates = [];

        state.srt_liv_rawLocalRouteData = {};
        state.srt_liv_osmWayNodeList = [];
        state.srt_liv_tagFeatureList = [];
        state.srt_liv_directionInstruction = [];
        state.srt_liv_elevation = [];
        state.srt_liv_polylineCoordinates = [];

        state.fts_liv_rawLocalRouteData = {};
        state.fts_liv_osmWayNodeList = [];
        state.fts_liv_tagFeatureList = [];
        state.fts_liv_directionInstruction = [];
        state.fts_liv_elevation = [];
        state.fts_liv_polylineCoordinates = [];

        state.rec_liv_rawLocalRouteData = {};
        state.rec_liv_osmWayNodeList = [];
        state.rec_liv_tagFeatureList = [];
        state.rec_liv_directionInstruction = [];
        state.rec_liv_elevation = [];
        state.rec_liv_polylineCoordinates = [];

        state.gcp_rawdata = {};
        state.gcp_polylineCoordinates = [];
        state.gcp_directionInstruction = [];
    },

    PUSH_COORDINATE_MARKER_LOCATION(state, payload) {
        state.markerList.push(payload);
    },

    SET_LOCAL_ROUTING_COORDINATES(state, payload) {
        console.log(payload);
        state.localPolylineCoordinates = payload;
    },
    SET_LIVE_ROUTING_COORDINATES(state, payload) {
        state.livePolylineCoordinates = payload;
    },

    // Toggle the Barrier Option for Adding a new Barrier Coordinates 
    SET_BARRIER_TOGGLE_OPTION(state, payload) {
        state.barrierToggle = payload.curState;
    },

    SET_BARRIER_COORDINATES(state, payload) {
        state.barrierCoordinates.push(payload);
    },

    SET_BUFFERED_BARRIER_COORDINATES(state, payload) {
        state.bufferedBarrierCoordinates.push(payload);
        // state.bufferedPayloadBarrierCoordinates.push([payload]);
    },

    // Adding the Barrier Coordinates for Avoidance
    SET_AVOID_BARRIER_COORDINATES(state, payload) {
        state.barrierCoordinates = payload;
    },

    //LOCAL ORS MUTATIONS FOR ANY PROFILE SELECTED
    // Shortest
    SET_SRT_LCL_RAWDATA(state, payload) {
        state.srt_lcl_rawLocalRouteData = payload;
    },
    SET_SRT_LCL_ROUTING_COORDINATES(state, payload) {
        state.srt_lcl_polylineCoordinates = payload;
    },
    SET_SRT_LCL_WAYNODE_LIST(state, payload) {
        state.srt_lcl_osmWayNodeList = payload;
    },
    SET_SRT_LCL_FEATURE_LIST(state, payload) {
        state.srt_lcl_tagFeatureList = payload;
    },

    SET_SRT_LCL_ROUTE_INSTRUCTION(state, payload) {
        state.srt_lcl_directionInstruction = payload;
    },

    SET_SRT_LCL_ROUTE_ELEVATION(state, payload) {
        state.srt_lcl_elevation = payload;
    },


    // Fastest
    SET_FTS_LCL_RAWDATA(state, payload) {
        state.fts_lcl_rawLocalRouteData = payload;
    },
    SET_FTS_LCL_ROUTING_COORDINATES(state, payload) {
        state.fts_lcl_polylineCoordinates = payload;
    },
    SET_FTS_LCL_WAYNODE_LIST(state, payload) {
        state.fts_lcl_osmWayNodeList = payload;
    },
    SET_FTS_LCL_FEATURE_LIST(state, payload) {
        state.fts_lcl_tagFeatureList = payload;
    },

    SET_FTS_LCL_ROUTE_INSTRUCTION(state, payload) {
        state.fts_lcl_directionInstruction = payload;
    },

    SET_FTS_LCL_ROUTE_ELEVATION(state, payload) {
        state.fts_lcl_elevation = payload;
    },

    // Recommended
    SET_REC_LCL_RAWDATA(state, payload) {
        state.rec_lcl_rawLocalRouteData = payload;
    },
    SET_REC_LCL_ROUTING_COORDINATES(state, payload) {
        state.rec_lcl_polylineCoordinates = payload;
    },
    SET_REC_LCL_WAYNODE_LIST(state, payload) {
        state.rec_lcl_osmWayNodeList = payload;
    },
    SET_REC_LCL_FEATURE_LIST(state, payload) {
        state.rec_lcl_tagFeatureList = payload;
    },
    SET_REC_LCL_ROUTE_INSTRUCTION(state, payload) {
        state.rec_lcl_directionInstruction = payload;
    },
    SET_REC_LCL_ROUTE_ELEVATION(state, payload) {
        state.rec_lcl_elevation = payload;
    },

    //LIVE ORS MUTATIONS FOR ANY PROFILE SELECTED
    // Shortest
    SET_SRT_LIV_RAWDATA(state, payload) {
        state.srt_liv_rawLocalRouteData = payload;
    },
    SET_SRT_LIV_ROUTING_COORDINATES(state, payload) {
        state.srt_liv_polylineCoordinates = payload;
    },
    SET_SRT_LIV_WAYNODE_LIST(state, payload) {
        state.srt_liv_osmWayNodeList = payload;
    },
    SET_SRT_LIV_FEATURE_LIST(state, payload) {
        state.srt_liv_tagFeatureList = payload;
    },
    SET_SRT_LIV_ROUTE_INSTRUCTION(state, payload) {
        state.srt_liv_directionInstruction = payload;
    },
    SET_SRT_LIV_ROUTE_ELEVATION(state, payload) {
        state.srt_liv_elevation = payload;
    },

    // Fastest
    SET_FTS_LIV_RAWDATA(state, payload) {
        state.fts_liv_rawLocalRouteData = payload;
    },
    SET_FTS_LIV_ROUTING_COORDINATES(state, payload) {
        state.fts_liv_polylineCoordinates = payload;
    },
    SET_FTS_LIV_WAYNODE_LIST(state, payload) {
        state.fts_liv_osmWayNodeList = payload;
    },
    SET_FTS_LIV_FEATURE_LIST(state, payload) {
        state.fts_liv_tagFeatureList = payload;
    },
    SET_FTS_LIV_ROUTE_INSTRUCTION(state, payload) {
        state.fts_liv_directionInstruction = payload;
    },
    SET_FTS_LIV_ROUTE_ELEVATION(state, payload) {
        state.fts_liv_elevation = payload;
    },

    // Recommended
    SET_REC_LIV_RAWDATA(state, payload) {
        state.rec_liv_rawLocalRouteData = payload;
    },
    SET_REC_LIV_ROUTING_COORDINATES(state, payload) {
        state.rec_liv_polylineCoordinates = payload;
    },
    SET_REC_LIV_WAYNODE_LIST(state, payload) {
        state.rec_liv_osmWayNodeList = payload;
    },
    SET_REC_LIV_FEATURE_LIST(state, payload) {
        state.rec_liv_tagFeatureList = payload;
    },
    SET_REC_LIV_ROUTE_INSTRUCTION(state, payload) {
        state.rec_liv_directionInstruction = payload;
    },
    SET_REC_LIV_ROUTE_ELEVATION(state, payload) {
        state.rec_liv_elevation = payload;
    },

    // No Attribute
    SET_NOATTR_RAWDATA(state, payload) {
        state.noattr_rawLocalRouteData = payload;
    },
    SET_NOATTR_ROUTING_COORDINATES(state, payload) {
        state.noattr_polylineCoordinates = payload;
    },
    SET_NOATTR_WAYNODE_LIST(state, payload) {
        state.noattr_osmWayNodeList = payload;
    },
    SET_NOATTR_FEATURE_LIST(state, payload) {
        state.noattr_tagFeatureList = payload;
    },
    SET_NOATTR_ROUTE_INSTRUCTION(state, payload) {
        state.noattr_directionInstruction = payload;
    },
    SET_NOATTR_ROUTE_ELEVATION(state, payload) {
        state.noattr_elevation = payload;
    },


    // GOOGLE DIRECTION API MUTATORS
    SET_GCP_RAWDATA(state, payload) {
        state.gcp_rawdata = payload;
    },
    SET_GCP_ROUTING_COORDINATES(state, payload) {
        state.gcp_polylineCoordinates = payload;
    },
    SET_GCP_ROUTING_INSTRUCTION(state, payload) {

    },
    SET_GCP_ROUTE_ELEVATION(state, payload) { },

    SET_REC_LCL_RUNNING_ANALYSIS(state, payload) {
        state.rec_lcl_analysis = payload;
    },
    SET_REC_LIV_RUNNING_ANALYSIS(state, payload) {
        state.rec_liv_analysis = payload;
    },
    SET_NOATTR_RUNNING_ANALYSIS(state, payload) {
        state.noattr_analysis = payload;
    },
    SET_WAC_UNCORRECTED_ANALYSIS(state, payload) {
        state.wacUncorrectedData = payload;
    },
    SET_WAC_CORRECTED_ANALYSIS(state, payload) {
        state.wacCorrectedData = payload;
    },
    SET_WAC_NOATTR_ANALYSIS(state, payload) {
        state.wacNoAttrData = payload;
    },
    SET_WAC_UNCORRECTED_SCORE(state, payload) {
        state.wacUncorrectedFinalScore = payload;
    },
    SET_WAC_CORRECTED_SCORE(state, payload) {
        state.wacCorrectedFinalScore = payload;
    },
    SET_WAC_NOATTR_SCORE(state, payload) {
        state.wacNoAttrFinalScore = payload;
    },
    SET_RAW_WAC_CORRECTED_DATA(state, payload) {
        state.rawCorrectedWacData = payload;
    },
    SET_RAW_WAC_UNCORRECTED_DATA(state, payload) {
        state.rawUncorrectedWacData = payload;
    },
    SET_RAW_WAC_NOATTR_DATA(state, payload) {
        state.rawNoAttrWacData = payload;
    },
    SET_COR_ROUTING_NODE_BUCKET(state, payload) {
        state.osmCorrectedNodeBucket = payload;
    },
    SET_UNCOR_ROUTING_NODE_BUCKET(state, payload) {
        state.osmUncorrectedNodeBucket = payload;
    },
    SET_NOATTR_ROUTING_NODE_BUCKET(state, payload) {
        state.osmNoattrNodeBucket = payload;
    }
};
