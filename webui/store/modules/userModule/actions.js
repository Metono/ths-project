const axios = require("axios");
export default {
    async GET_USER_PROFILE_DETAILS({ commit }) {
        try {
            let myResult = await this.$axios.$get('/api/users/me',
                { headers: { Authorization: `Bearer ${this.$auth.strategy.token.get()}` } }
            );
            if (myResult) {
                let myProfile = await this.$axios.$get(`/api/profile/userdetails`, { headers: { Authorization: `Bearer ${this.$auth.strategy.token.get()}` } })
                commit("SET_USER_PROFILE_DETAILS", myProfile.data);
                return myProfile;
            }
        } catch (err) {
            console.log(err);
        }
    },
    async SAVE_USER_PROFILE_DETAILS({ commit }, payload){
        try{
            console.log(payload);
        }catch (err){
            console.log(err);
        }
    }
};
