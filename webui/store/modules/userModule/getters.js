export default {
    GET_USER_PROFILE_DETAILS(state) {
        return state.userProfile;
    },
    GET_PROFILE_ROUTING_TYPE(state) {
        if (state.userProfile.profiletype == "walking") {
            return "foot-walking"
        } else if (state.userProfile.profiletype == "wheelchair") {
            return "wheelchair"
        } else {
            return "foot-walking"
        }
    },
    GET_PROFILE_ROUTING_PREFERENCE(state) {
        if (state.userProfile.routingtype == "shortest") {
            return "shortest"
        } else if (state.userProfile.routingtype == "fastest") {
            return "fastest"
        } else {
            return "recomended"
        }
    }
}