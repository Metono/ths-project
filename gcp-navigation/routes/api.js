// Initalize Libraries
const { Router } = require("express");
// Create Instance
const router = new Router();

// Initialize controllers
const apiController = require("../controllers/apiController");

// Update User Profile Dark Mode
router.post("/api/generate", apiController.generateGoogleRoute);

// Export modules
module.exports = router;