require("dotenv").config();

const express = require("express");
const cors = require("cors");

// Logger
const logger = require('./logger');

// Create express instance
const app = express();


// Initialize json bosy options body-parser library
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// CALL MIDDLEWARES
app.use(cors());

// Require route API
const apiRoutes = require("./routes/api");

// ENABLE API ROUTES
app.use(apiRoutes);

const SERVER_PORT = process.env.NODE_PORT || 8200;

app.listen(SERVER_PORT, () => {
  logger.info(`Server Listening on Port : ${SERVER_PORT}`);
});