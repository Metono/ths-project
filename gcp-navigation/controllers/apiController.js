require("dotenv").config();
const logger = require('../logger');
const axios = require("axios");
module.exports.generateGoogleRoute = async function (req, res) {
    try {
      
        let { origin, destination } = req.body;
        console.log(origin);
        console.log(destination);
        let myResult = await axios.get(`https://maps.googleapis.com/maps/api/directions/json?language=en&mode=walking&destination=${destination}&origin=${origin}&key=${process.env.GCP_API_KEY}`);
        if(myResult){
            return res.status(200).json(myResult.data);
        }
    } catch (err) {
        logger.error(`[generateGoogleRoute] Error In Generating Route`);
        logger.error(err.message);
        return res.status(500).json(err.message);
    }
};