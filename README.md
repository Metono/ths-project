#Setup Main Containers

Run the Command on the terminal

Open the project directory
cd <YOUR PROJECT DIRECTORY>
docker-compose up --build
Ctrl-C
docker-compose up

Open a new terminal and run the local copy of the osm server.
This command will copy the existing data of switzerland from osm server and save it locally to your machine. 
Currently from this writing the file is around 600MB and after the project has finished building it would be around 6GB.

# Setup for local OpenRouteService

Things to note

- Delete the readme files inside the individual folders inside appdata folder
- Copy the file "switzerland-latest.osm.pbf" and paste it to the 
- To Run the overpass api server locally run this docker command in the terminal


# Setup for LOCAL QGIS OSM Data

## Can by any of the Two Applications using osm2pgsql or osmosis

### OSM2PGSQL Server
Following Tutorial in https://learnosm.org/en/osm-data/osm2pgsql/

Download pgsql2osm
FOR Importing Local Data before data hydration
RUN this script on the cmd

osm2pgsql -c -d osm_test -U postgres -W -H localhost -j -x -S %UserProfile%\Documents\ths-project\mapdata\osm-custom.style --extra-attributes --slim %UserProfile%\Documents\ths-project\mapdata\corrected_kreis1_17052022.osm.pbf


FOR Importing Local Data after data hydration
RUN this script on the cmd

osm2pgsql -c -d osm_test -U postgres -W -H localhost -j -x -S %UserProfile%\Documents\ths-project\mapdata\osm-custom.style --extra-attributes --slim %UserProfile%\Documents\ths-project\mapdata\corrected_kreis1_17052022.osm.pbf


## Osmosis
### Make a Database in your PGAdmin

### Then Add Extensions by running the scripts in PGAdmin
````
CREATE EXTENSION hstore;
CREATE EXTENSION postgis;
````

### Run The Following Scripts in PGAdmin This will create the OSM Schema using Simple schema but you can use the phsnapshot if you want
````
-- Database creation script for the snapshot PostgreSQL schema.

-- Drop all tables if they exist.
DROP TABLE IF EXISTS actions;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS nodes;
DROP TABLE IF EXISTS ways;
DROP TABLE IF EXISTS way_nodes;
DROP TABLE IF EXISTS relations;
DROP TABLE IF EXISTS relation_members;
DROP TABLE IF EXISTS schema_info;

-- Drop all stored procedures if they exist.
DROP FUNCTION IF EXISTS osmosisUpdate();


-- Create a table which will contain a single row defining the current schema version.
CREATE TABLE schema_info (
    version integer NOT NULL
);


-- Create a table for users.
CREATE TABLE users (
    id int NOT NULL,
    name text NOT NULL
);


-- Create a table for nodes.
CREATE TABLE nodes (
    id bigint NOT NULL,
    version int NOT NULL,
    user_id int NOT NULL,
    tstamp timestamp without time zone NOT NULL,
    changeset_id bigint NOT NULL,
    tags hstore
);
-- Add a postgis point column holding the location of the node.
SELECT AddGeometryColumn('nodes', 'geom', 4326, 'POINT', 2);


-- Create a table for ways.
CREATE TABLE ways (
    id bigint NOT NULL,
    version int NOT NULL,
    user_id int NOT NULL,
    tstamp timestamp without time zone NOT NULL,
    changeset_id bigint NOT NULL,
    tags hstore,
    nodes bigint[]
);


-- Create a table for representing way to node relationships.
CREATE TABLE way_nodes (
    way_id bigint NOT NULL,
    node_id bigint NOT NULL,
    sequence_id int NOT NULL
);


-- Create a table for relations.
CREATE TABLE relations (
    id bigint NOT NULL,
    version int NOT NULL,
    user_id int NOT NULL,
    tstamp timestamp without time zone NOT NULL,
    changeset_id bigint NOT NULL,
    tags hstore
);

-- Create a table for representing relation member relationships.
CREATE TABLE relation_members (
    relation_id bigint NOT NULL,
    member_id bigint NOT NULL,
    member_type character(1) NOT NULL,
    member_role text NOT NULL,
    sequence_id int NOT NULL
);


-- Configure the schema version.
INSERT INTO schema_info (version) VALUES (6);


-- Add primary keys to tables.
ALTER TABLE ONLY schema_info ADD CONSTRAINT pk_schema_info PRIMARY KEY (version);

ALTER TABLE ONLY users ADD CONSTRAINT pk_users PRIMARY KEY (id);

ALTER TABLE ONLY nodes ADD CONSTRAINT pk_nodes PRIMARY KEY (id);

ALTER TABLE ONLY ways ADD CONSTRAINT pk_ways PRIMARY KEY (id);

ALTER TABLE ONLY way_nodes ADD CONSTRAINT pk_way_nodes PRIMARY KEY (way_id, sequence_id);

ALTER TABLE ONLY relations ADD CONSTRAINT pk_relations PRIMARY KEY (id);

ALTER TABLE ONLY relation_members ADD CONSTRAINT pk_relation_members PRIMARY KEY (relation_id, sequence_id);


-- Add indexes to tables.
CREATE INDEX idx_nodes_geom ON nodes USING gist (geom);

CREATE INDEX idx_way_nodes_node_id ON way_nodes USING btree (node_id);

CREATE INDEX idx_relation_members_member_id_and_type ON relation_members USING btree (member_id, member_type);


-- Set to cluster nodes by geographical location.
ALTER TABLE ONLY nodes CLUSTER ON idx_nodes_geom;

-- Set to cluster the tables showing relationship by parent ID and sequence
ALTER TABLE ONLY way_nodes CLUSTER ON pk_way_nodes;
ALTER TABLE ONLY relation_members CLUSTER ON pk_relation_members;

-- There are no sensible CLUSTER orders for users or relations.
-- Depending on geometry columns different clustings of ways may be desired.

-- Create the function that provides "unnest" functionality while remaining compatible with 8.3.
CREATE OR REPLACE FUNCTION unnest_bbox_way_nodes() RETURNS void AS $$
DECLARE
	previousId ways.id%TYPE;
	currentId ways.id%TYPE;
	result bigint[];
	wayNodeRow way_nodes%ROWTYPE;
	wayNodes ways.nodes%TYPE;
BEGIN
	FOR wayNodes IN SELECT bw.nodes FROM bbox_ways bw LOOP
		FOR i IN 1 .. array_upper(wayNodes, 1) LOOP
			INSERT INTO bbox_way_nodes (id) VALUES (wayNodes[i]);
		END LOOP;
	END LOOP;
END;
$$ LANGUAGE plpgsql;


-- Create customisable hook function that is called within the replication update transaction.
CREATE FUNCTION osmosisUpdate() RETURNS void AS $$
DECLARE
BEGIN
END;
$$ LANGUAGE plpgsql;

-- Manually set statistics for the way_nodes and relation_members table
-- Postgres gets horrible counts of distinct values by sampling random pages
-- and can be off by an 1-2 orders of magnitude

-- Size of the ways table / size of the way_nodes table
ALTER TABLE way_nodes ALTER COLUMN way_id SET (n_distinct = -0.08);

-- Size of the nodes table / size of the way_nodes table * 0.998
-- 0.998 is a factor for nodes not in ways
ALTER TABLE way_nodes ALTER COLUMN node_id SET (n_distinct = -0.83);

-- API allows a maximum of 2000 nodes/way. Unlikely to impact query plans.
ALTER TABLE way_nodes ALTER COLUMN sequence_id SET (n_distinct = 2000);

-- Size of the relations table / size of the relation_members table
ALTER TABLE relation_members ALTER COLUMN relation_id SET (n_distinct = -0.09);

-- Based on June 2013 data
ALTER TABLE relation_members ALTER COLUMN member_id SET (n_distinct = -0.62);

-- Based on June 2013 data. Unlikely to impact query plans.
ALTER TABLE relation_members ALTER COLUMN member_role SET (n_distinct = 6500);

-- Based on June 2013 data. Unlikely to impact query plans.
ALTER TABLE relation_members ALTER COLUMN sequence_id SET (n_distinct = 10000);
````



## And Then
````
-- Add a postgis GEOMETRY column to the way table for the purpose of storing the full linestring of the way.
SELECT AddGeometryColumn('ways', 'linestring', 4326, 'GEOMETRY', 2);

-- Add an index to the bbox column.
CREATE INDEX idx_ways_linestring ON ways USING gist (linestring);

-- Cluster table by geographical location.
CLUSTER ways USING idx_ways_linestring;
````

### When Everything has been formtted and the schema has been updated
you can now run the import script

## Import Script

```
osmosis --read-pbf %UserProfile%\Documents\ths-project\mapdata\YOUR_OSM_PBF_FILE.osm.pbf --log-progress --write-pgsql user="postgres" password="postgres" database="YOUR_DB_NAME"
```

## Export Script

```
osmosis --read-pgsql host="localhost" database="osm_inclineddb" user="postgres" password="postgres" --dataset-dump --write-xml file="kreis1_24042022_incline.osm"
```

# Setup for Local Overpass API

Things to Note:

Running this service will download the OSM data for switzerland with around  700MB as of 25/03/2022.
After downloading this will process the data and make a graph wait until done estimated time is around 10-40 minutes depending on your server hardware.

# QGIS Formulas
Converting Z Raster Data to Degrees
This is applied on the table using field calculator
```
round(tan("first"),2)
```